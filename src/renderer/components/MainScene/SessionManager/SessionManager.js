import date from 'date-and-time'



import {
    ipcRenderer
} from 'electron'

const arrAvg = arr => arr.reduce((a, b) => a + b, 0) / arr.length

class SessionManager {

    constructor() {

        // Fires when uploaded
        ipcRenderer.on('session-reply', (event, arg) => {
            //console.log(arg) // prints "pong"
            this.clearSessionData();
        })
        // Fires when uploaded

        ipcRenderer.on('sample-reply', (event, arg) => {
            //console.log(arg) // prints "pong"
            this.clearSampleData();
        })

        this.state = 0;
        this.audio_level = {
            current: 0,
            average: [],
            scene1: [],
            scene2: [],
            scene3: [],
        };
        this.motion_speed = {
            current: 0,
            average: [],
            scene1: [],
            scene2: [],
            scene3: [],
        }


        this.time = 0.0;

        // Session JSON
        this.sessionData = {}
        this.clearSessionData();

        let now = new Date();

        //Sample JSON
        this.sampleData = {
            date: date.format(now, 'YYYY/MM/DDTHH:mm:ssZ'),
            audio_level: this.audio_level,
            motion_speed: this.motion_speed,
            current_scene: this.state,
            skeletons: null
        }

    }



    clearSessionData() {
        let now = new Date();
        this.time = performance.now();
        this.sessionTime = performance.now();
        this.sessionData = {}
        this.sessionData.date = date.format(now, 'YYYY/MM/DDTHH:mm:ssZ');
        // this.sessionData.date = "2013-07-23T01:18:32.000Z";

        this.sessionData.average_audio_level = 0;
        this.sessionData.time_active = 0;
        this.sessionData.time_inactive = 0;
        this.sessionData.scene1 = {
            time_active: 0,
            key_interaction: 0,
            special_interaction: 0,
            average_motion_speed: 0,
            average_audio_level: 0
        }
        this.sessionData.scene2 = {
            time_active: 0,
            key_interaction: 0,
            special_interaction: 0,
            average_motion_speed: 0,
            average_audio_level: 0
        }
        this.sessionData.scene3 = {
            time_active: 0,
            key_interaction: 0,
            special_interaction: 0,
            average_motion_speed: 0,
            average_audio_level: 0
        }

        this.audio_level = {
            current: 0,
            average: [],
            scene1: [],
            scene2: [],
            scene3: [],
        };
        this.motion_speed = {
            current: 0,
            average: [],
            scene1: [],
            scene2: [],
            scene3: [],
        }


    }

    clearSampleData() {
        let now = new Date();

        this.audio_level = {
            current: 0,
            average: [],
            scene1: [],
            scene2: [],
            scene3: [],
        };
        this.motion_speed = {
            current: 0,
            average: [],
            scene1: [],
            scene2: [],
            scene3: [],
        }
        this.sampleData = {
            date: date.format(now, 'YYYY/MM/DDTHH:mm:ssZ'),
            audio_level: this.audio_level,
            motion_speed: this.motion_speed,
            current_scene: this.state,
            skeletons: null
        }
    }

    setAudioLevel(audiolevel) {
        this.audio_level.current = audiolevel
        this.audio_level.average.push(audiolevel)
        ////console.log("audiolevel", audiolevel);

        switch (this.state) {
            case 1:
                this.audio_level.scene1.push(audiolevel)
                this.sessionData.scene1.average_audio_level = arrAvg(this.audio_level.scene1)
                break;
            case 2:
                this.audio_level.scene2.push(audiolevel)
                this.sessionData.scene2.average_audio_level = arrAvg(this.audio_level.scene2)

                break;
            case 3:
                this.audio_level.scene3.push(audiolevel)
                this.sessionData.scene3.average_audio_level = arrAvg(this.audio_level.scene3)
                if(audiolevel > 0.6) {
                    this.sessionData.scene3.key_interaction += 1;
                }
                break;
            default:
                break;
        }

        this.sessionData.average_audio_level = (this.sessionData.scene1.average_audio_level + this.sessionData.scene2.average_audio_level +
            this.sessionData.scene3.average_audio_level);
    }


    setMotionSpeed(motionspeed) {
        this.motion_speed.current = motionspeed
        this.motion_speed.average.push(motionspeed);
        switch (this.state) {
            case 1:
                this.motion_speed.scene1.push(motionspeed)
                this.sessionData.scene1.average_motion_speed = arrAvg(this.motion_speed.scene1)
                break;
            case 2:
                this.motion_speed.scene2.push(motionspeed)
                this.sessionData.scene2.average_motion_speed = arrAvg(this.motion_speed.scene2)

                break;
            case 3:
                this.motion_speed.scene3.push(motionspeed)
                this.sessionData.scene3.average_motion_speed = arrAvg(this.motion_speed.scene3)
                break;
            default:
                break;
        }

    }

    setState(state) {
        this.setSessionTime(this.state, Math.abs(this.sessionTime - performance.now()));
        this.sessionTime = performance.now();
        this.state = state;
    }

    setKeyInteraction(state) {
        switch (state) {
            case 0:
                break;
            case 1:
                this.sessionData.scene1.key_interaction += 1;
                break;
            case 2:
                this.sessionData.scene2.key_interaction += 1;

                break;
            case 3:
                this.sessionData.scene3.key_interaction += 1;

                break;
            default:
                break;
        }

    }
    setSessionTime(state, time) {
        if (!isNaN(time)) {


            switch (state) {
                case 0:
                    this.sessionData.time_inactive += time;
                    break;
                case 1:
                    this.sessionData.scene1.time_active += time;
                    this.sessionData.time_active += time;

                    break;
                case 2:
                    this.sessionData.scene2.time_active += time;
                    this.sessionData.time_active += time;

                    break;
                case 3:
                    this.sessionData.scene3.time_active += time;
                    this.sessionData.time_active += time;

                    break;
                default:
                    break;
            }

        }
        ////console.log(this.state, this.sessionData.time_active);

    }


    // Sent via ipc to the main process.
    uploadSessionData() {
        // ipcRenderer.send('session-data', this.sessionData)
        // ////console.log("session data ", this.sessionData);
    }


    uploadSampleData(_skeletons) {
        this.sampleData = {
            date: date.format(new Date(), 'YYYY/MM/DDTHH:mm:ssZ'),
            audio_level: this.audio_level.current,
            motion_speed: this.motion_speed.current,
            current_scene: this.state,
            skeletons: []
        }
        ////console.log("this.sampleData",this.sampleData.audio_level);
        for (let i = 0, l = _skeletons.length; i < l; i++) {
            if (_skeletons[i]) {
                if (_skeletons[i].pose.score > 0.3) {
                    this.sampleData.skeletons.push({
                        label: 'person' + (i + 1).toString(),
                        keypoints: _skeletons[i].pose.keypoints
                    });
                }
            }
        }
        // ipcRenderer.send('sample-data', this.sampleData);

        // //console.log('sample-data', this.sampleData);
    }








}

export default new SessionManager();