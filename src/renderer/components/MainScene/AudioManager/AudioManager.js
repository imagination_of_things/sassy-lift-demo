import Tone from 'tone'
import {
    Math
} from 'three'

// import SessionManager from 
class AudioManager {

    constructor() {
        // this.initAudioListener();
        this.level = 0.0;
        this.midLevel = 0.0;
        this.mic = new Tone.UserMedia();
        this.bassMeter = new Tone.Meter();
        this.midMeter = new Tone.Meter();
        this.trebleMeter = new Tone.Meter();

        this.followerMeter = new Tone.Meter();
        this.followerMeter2 = new Tone.Meter();
        this.followerMeter3 = new Tone.Meter();

        this.followerMeter4 = new Tone.Meter();
        this.followerMeter5 = new Tone.Meter();
        this.followerMeter6 = new Tone.Meter();

        this.inited = false;
        this.state = 0;
        // Filters 
        this.bass = new Tone.Filter({
            type: "lowpass",
            frequency: 350,
            rolloff: -12,
            Q: 2,
            gain: 0
        });
        this.mid = new Tone.Filter({
            type: "bandpass",
            frequency: 900,
            Q: 0.4,
            gain: 6
        });
        this.treble = new Tone.Filter({
            type: "highpass",
            frequency: 1000,
            rolloff: -12,
            Q:2,
            gain: 16
        });

        this.follower = new Tone.Follower(0.55);
        this.follower2 = new Tone.Follower(0.4);
        this.follower3 = new Tone.Follower(0.5);
        this.follower4 = new Tone.Follower(0.7);
        this.follower5 = new Tone.Follower(0.65);
        this.follower6 = new Tone.Follower(0.6);


    }

    initAudioListener() {
        this.mic.open().then(() => {
            //promise resolves when input is available
            this.mic.fan(this.bass, this.mid, this.treble);
            this.bass.fan(this.bassMeter, this.follower, this.follower4);
            this.mid.fan(this.midMeter, this.follower2, this.follower5, this.follower3, this.follower6);
            this.treble.fan(this.trebleMeter);
            this.follower.connect(this.followerMeter);
            this.follower2.connect(this.followerMeter2);
            this.follower3.connect(this.followerMeter3);
            this.follower4.connect(this.followerMeter4);
            this.follower5.connect(this.followerMeter5);
            this.follower6.connect(this.followerMeter6);
            
            this.inited = true;
            // this.level = this.meter.getLevel();
        });


    }

    getAudioLevel() {

        if (this.inited) {
            // this.midLevel = Math.clamp(this.midLevel, 1, 0);
            return [this.followerMeter.getLevel(), this.followerMeter2.getLevel(), this.followerMeter3.getLevel(), this.followerMeter4.getLevel(), this.followerMeter5.getLevel(), this.followerMeter6.getLevel()];
        } else {
            return 0.0
        }
    }

    getSimplerAudioLevel() {
        if (this.inited) {
            this.midLevel = Math.mapLinear(this.midMeter.getLevel(), -80, -10, 0, 1);
            this.midLevel = Math.clamp(this.midLevel, 0, 1);       
            return this.midLevel;
        }
        else {
            return 0.0
        }
    }


    getBassMidTreble() {

        if (this.inited) {
            this.bassLevel = Math.mapLinear(this.bassMeter.getLevel(), -80, 0, 0, 1);
            this.bassLevel = Math.clamp(this.midLevel, 0, 1);

            this.midLevel = Math.mapLinear(this.midMeter.getLevel(), -80, 0, 0, 1);
            this.midLevel = Math.clamp(this.midLevel, 0, 1);

            this.trebleLevel = Math.mapLinear(this.trebleMeter.getLevel(), -80, 0, 0, 1);
            this.trebleLevel = Math.clamp(this.midLevel, 0, 1);
            return [this.bassLevel, this.midLevel, this.trebleLevel];
        } else {
            return [0.0,0.0,0.0]
        }
    }










}

export default new AudioManager();