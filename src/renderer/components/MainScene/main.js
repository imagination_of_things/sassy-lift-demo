// import * as THREE from 'three'
import SceneManager from './SceneManager/SceneManager'
import ModelLoader from './SceneManager/ModelLoader'
import PoseManager from './PoseManager/PoseManager.js'
import OSCManager from './OSCManager/OSCManager'
import SessionManager from './SessionManager/SessionManager'
import AudioManager from './AudioManager/AudioManager'
import Stats from 'stats-js'
import * as THREE from 'three'


// TEXT

// import  from '../Text/Scene1Text.vue'

import {
    ipcRenderer
} from 'electron'

export default {
    name: 'Scene',

    data() {
        return {
            sceneManager: null,
            poseManager: null,
            loading: false,
            debug: false,
            state: 0,
            activity: 0,
            LT_activity: 0,
            player: null,
            handsUp: false,
            modelsLoaded: false,
            stats: null,
            shouldChange: true,
            poseNetHasLoaded: false,

        }
    },

    mounted() {
        // For debug listen to keyevents
        window.addEventListener("keypress", e => {
            this.manualSetState(String.fromCharCode(e.keyCode));
            this.setGameState(String.fromCharCode(e.keyCode));

        });



        // Here we get the promise back from bacjground process.
        ipcRenderer.on('threejs-posenet-loaded', (event) => {
            console.log(event);
            console.log("posenet loaded from hidden window");

            this.poseManager = new PoseManager(this.activityMonitor, this.onNewPosition);
            this.modelLoader = ModelLoader;
            document.getElementById('loading-posenet').style.display = 'none';

            // // This also contains a promise
            this.initModels();
            this.poseNetHasLoaded = true;



        });

        ipcRenderer.on('posenet-data', (event, data) => {
            if (this.poseManager && this.sceneManager) {
                this.poseManager.getPosesFromOtherWindow(data);
            }
        });

    },

    methods: {


        initSceneManager: function () {
            var canvas = document.getElementById("canvas");
            this.sceneManager = new SceneManager(canvas);


            // this.stats = new Stats();
            // this.stats.showPanel(0); // 0: fps, 1: ms, 2: mb, 3+: custom
            // this.stats.showPanel( 1 ); // 0: fps, 1: ms, 2: mb, 3+: custom
            this.clock = new THREE.Clock();
            // document.body.appendChild(this.stats.dom);




        },

        initModels: function () {

            this.modelLoader.loadFiles().then(() => {
                this.modelsLoaded = true;
                console.log("GLTF models loaded");
                this.initSceneManager();
                this.initOSC();
                this.render();
                AudioManager.initAudioListener();
                setInterval(() => {
                    // if (AudioManager.inited) {
                    SessionManager.setAudioLevel(AudioManager.getAudioLevel());
                    // console.log(AudioManager.getAudioLevel())
                    // }
                }, 100);





                // this.demoSequence();
                // Test ping




                // Remove loading screen
                document.getElementById('loading-models').style.display = 'none';
                // this.initAudio();
            }).catch((error) => {
                console.log(error);
            })
        },

        initOSC: function () {
            this.oscManager = OSCManager;

        },

        scene3Listen: function () {

        },


        demoSequence: function () {
            setInterval(() => {
                if (this.state < 4) {
                    this.state += 1;
                } else {
                    this.state = 0;
                }

                this.manualSetStateNum(this.state % 4)
                console.log(this.state % 4);
            }, 20000)

        },

        // initAudio: function () {
        //     this.audioManager = new AudioManager();
        // },

        // // Callback from Posemanager when new pose data
        onNewPosition: function (positions) {
            // console.log("");

            if (this.modelsLoaded && positions) {
                if (this.sceneManager) {
                    this.sceneManager.setPositions(positions);

                }

            }
        },

        // // Follow the activity from PoseManager to inform Scene manager
        activityMonitor: function (activityLevel, LT_activity) {
            // console.log(activityLevel, LT_activity);
            this.activity = activityLevel;
            this.LT_activity = LT_activity.toFixed(4);
            if (LT_activity === 0.0) {
                this.state = 0;
                this.sceneManager.setState(0);
                this.oscManager.sendState(0);
                this.shouldChange = true;

                // this.player.stop()

                if (this.sessionEnded) {
                    // SessionManager.uploadSessionData()
                    // console.log('session ended');
                    this.sessionEnded = false;
                }
            }





            if (this.shouldChange === true) {
                if (activityLevel > 0.05 && activityLevel <= 1.5 && this.state != 2) {
                    this.state = 1;
                    this.sceneManager.setState(1);
                    this.oscManager.sendState(1);

                    this.sessionEnded = true;

                } else if (activityLevel > 1.5 && activityLevel <= 2.5) {
                    this.state = 2;
                    this.sceneManager.setState(2);
                    this.oscManager.sendState(2);
                    this.sessionEnded = true;

                } else if (activityLevel > 2.5) {
                    this.shouldChange = false;
                    this.state = 3;
                    this.sceneManager.setState(3);
                    this.oscManager.sendState(3);
                    this.sessionEnded = true;

                }

                this.activity = activityLevel;
                this.sceneManager.setActivity(activityLevel);
            }

        },

        //Here is our render loop.
        render: function () {
            // this.stats.begin();
            // setTimeout( () => {

            // requestAnimationFrame( this.render );

            // }, 1000 /  );
            requestAnimationFrame(this.render);
            var delta = this.clock.getDelta();
            //   Setup AudioLevel Analysis


            // SessionManager.setAudioLevel

            if (delta < 1) {
                this.sceneManager.update(delta);

            }
            // this.stats.end();

        },

        manualSetStateNum: function (num) {
            this.state = num;
            switch (num) {
                case 0:
                    this.sceneManager.setState(0);
                    this.oscManager.sendState(0);
                    SessionManager.setState(0);
                    this.shouldChange = true;

                    break;
                case 1:
                    this.sceneManager.setState(1);
                    this.oscManager.sendState(1);
                    SessionManager.setState(1);


                    break;
                case 2:
                    this.sceneManager.setState(2);
                    this.oscManager.sendState(2);
                    SessionManager.setState(2);

                    break;
                case 3:
                    this.sceneManager.setState(3);
                    this.oscManager.sendState(3);
                    SessionManager.setState(3);
                    this.shouldChange = false;
                    //console.log(this.shouldChange);


                    break;
                default:
                    break;
            }
        },

        // For keys to debug
        manualSetState: function (key) {
            let num = parseInt(key);
            this.state = num;
            //console.log(SessionManager.state);

            switch (num) {
                case 0:
                    this.sceneManager.setState(0);
                    this.oscManager.sendState(0);
                    SessionManager.setState(0);
                    this.shouldChange = true;
                    SessionManager.uploadSessionData();
                    break;
                case 1:
                    this.sceneManager.setState(1);
                    this.oscManager.sendState(1);
                    SessionManager.setState(1);


                    break;
                case 2:
                    this.sceneManager.setState(2);
                    this.oscManager.sendState(2);
                    SessionManager.setState(2);

                    break;
                case 3:
                    this.sceneManager.setState(3);
                    this.oscManager.sendState(3);
                    SessionManager.setState(3);
                    this.shouldChange = false;
                    // console.log(this.shouldChange);

                    break;
                default:
                    break;
            }

        },

        setGameState: function (key) {

            //console.log(SessionManager.state);

            switch (key) {
                case "a":
                    this.sceneManager.setGameState(0);
                    break;
                case "s":
                    this.sceneManager.setGameState(1);
                    break;
                case "d":
                    this.sceneManager.setGameState(2);
                    break;
                case "p":
                    this.sceneManager.clap();
                    break;
                case "g":
                    this.sceneManager.grenade();
                    break;
                default:
                    break;
            }

        },


    }
}