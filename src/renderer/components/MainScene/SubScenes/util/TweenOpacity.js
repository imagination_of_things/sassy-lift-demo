import TWEEN from '@tweenjs/tween.js'


export default function fadeMesh(mesh, direction, options) {
    console.log("CALLED")
    options = options || {};
    // set and check 
    var current = { percentage : direction == "in" ? 1 : 0 },
    // this check is used to work with normal and multi materials.
    mats = mesh.material.materials ? 
             mesh.material.materials : [mesh.material],
 
     originals = mesh.userData.originalOpacities,
     easing = options.easing || TWEEN.Easing.Linear.None,
     duration = options.duration || 2000;
    // check to make sure originals exist
    if( !originals ) {
         console.error("Fade error: originalOpacities not defined, use trackOriginalOpacities");
          return;
    }
    // tween opacity back to originals
    var tweenOpacity = new TWEEN.Tween(current)
        .to({ percentage: direction == "in" ? 0 : 1 }, duration)
        .easing(easing)
        .onUpdate(function() {
                mesh.material = current.percentage;
             

         })
         .onComplete(function(){
              if(options.callback){
                   options.callback();
              }
         });
    tweenOpacity.start();
    return tweenOpacity;
}