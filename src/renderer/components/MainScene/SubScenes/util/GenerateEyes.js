import * as THREE from 'three'
import getRandom from '../libs/Utils'

export default function(position, size) {
    let eyeSpacing = 0.4
    var icoGeometry = new THREE.Geometry();
    icoGeometry.vertices.push(
        new THREE.Vector3( position.x +eyeSpacing, position.y, position.z ),
        new THREE.Vector3( position.x -eyeSpacing, position.y, position.z ),
    );
    var icoMaterial = new THREE.MeshBasicMaterial({
        transparent: true,
        opacity: 0.0,
        wireframe: false
    });
    var eyeNum = 0;
    var baseSphere = new THREE.Mesh(icoGeometry, icoMaterial);
    // baseSphere.visible = false;

    var envMap = new THREE.TextureLoader().load('static/textures/env2.png');
    envMap.mapping = THREE.SphericalReflectionMapping;

    var eyeTexture_red = new THREE.TextureLoader().load('static/textures/eye_red.jpg');
    eyeTexture_red.mapping = THREE.SphericalReflectionMapping;

    var eyeGeometry = new THREE.IcosahedronGeometry(size, 3);

    // modify UVs to accommodate texture
    var faceVertexUvs = eyeGeometry.faceVertexUvs[0];
    for (var i = 0; i < faceVertexUvs.length; i++) {

        var uvs = faceVertexUvs[i];
        var face = eyeGeometry.faces[i];

        for (var j = 0; j < 3; j++) {

            uvs[j].x = face.vertexNormals[j].x * 0.5 + 0.5;
            uvs[j].y = face.vertexNormals[j].y * 0.5 + 0.5;

        }
    }

    var eyeMaterial = new THREE.MeshStandardMaterial({
        color: "#aaaaaa",
        roughness: 0,
        metalness: .1,
        shading: THREE.SmoothShading,
        opacity: 1,
        transparent: true,


    });
    eyeMaterial.envMap = envMap;
    eyeMaterial.map = eyeTexture_red;

    var eye = baseSphere.clone();

    eye.geometry = eyeGeometry;
    eye.material = eyeMaterial;

    var eyes = new Array();
    var randFactors = new Array();
    eyeNum = icoGeometry.vertices.length;
    // add an eye on each vertex
    for (var i = 0; i < eyeNum; i++) {
        var vertex = icoGeometry.vertices[i];

        if (vertex.z < 0)
            continue;

        var tEye = eye.clone();

        tEye.position.set(vertex.x, vertex.y, vertex.z);

        eyes.push(tEye);
        randFactors.push({
            x: getRandom(-0.4, 0.4),
            y: getRandom(-0.4, 0.4)
        });
    }


    return eyes;



}