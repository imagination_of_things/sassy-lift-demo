import * as THREE from 'three'
import Simple1DNoise from '../util/Perlin'
import ModelLoader from '../../SceneManager/ModelLoader'
import Palette from './Colors'
import SessionManager from '../../SessionManager/SessionManager'
import {
    TweenMax,
    TweenLite
} from 'gsap'

import OSCManager from '../../OSCManager/OSCManager'

import AudioManager from '../../AudioManager/AudioManager'

export default class Scene2 {

    constructor(scene, camera) {

        // External
        this.sessionTime = 0.0;
        this.oscManager = OSCManager;
        this.generator = new Simple1DNoise();

        this.difference = 0.0;

        this.monsterPosState = 0;
        this.monster2PosState = 0;


        // Scene related
        this.sceneGroup2 = new THREE.Group();
        scene.add(this.sceneGroup2);
        this.sceneGroup2.visible = false;
        this.camera = camera;
        this.hidden = true;
        this.active = false;
        this.state = 123;
        this.cameraWidth = 940;
        this.cameraHeight = 300;
        this.score = 0;
        this.internalTime = 0;


        // STATES
        this.powerUpState = false;
        this.powerUpPlayState = false;
        this.handConnectionThreshold = 100.0;
        this.hasClapped = false;
        this.farState = false;

        this.monstersLoaded = false;


        //Projectiles 
        this.textureLoader = new THREE.TextureLoader();
        this.sprite = this.textureLoader.load('static/textures/particle/projectile.png');


        this.blinks = [];

        this.monsters = [];
        this.colorCounter = 0;

        this.colors = Palette;
        this.clock = new THREE.Clock();
        this.banana;
        this.bananaParticles = [];
        this.handSpheres = [];
        this.mixers = [];
        this.debris = [];

        this.makeHandSpheres();
        this.loadMonster();
        this.loadBanana();

    }


    // Hand spheres
    makeHandSpheres() {

        for (let i = 0; i < 4; i++) {
            let geometry = new THREE.TetrahedronGeometry(5, 1);
            var material = new THREE.MeshStandardMaterial({
                color: 0x00ff00,
                opacity: 0.0,
                transparent: true,
            });
            var sphere = new THREE.Mesh(geometry, material);
            sphere.scale.set(0.5, 0.5, 0.5);
            sphere.position.set(0, 0, 0);

            this.sceneGroup2.add(sphere);
            this.handSpheres.push(sphere);
        }
    }


    loadMonster() {
        // Get laser model
        let model = ModelLoader.models.find(model => {
            return model.name === 'laser'
        })

        // Set morph
        // model.gltf.scene.children[0].material.morphTargets = true;

        // Monster 1
        this.monster = model.gltf;
        this.sceneGroup2.add(this.monster.scene);
        this.monster.scene.position.set(-100, 15, -20);
        this.monster.scene.scale.set(15, 15, 15);


        // Animations Mixer
        this.monsterMixer1 = new THREE.AnimationMixer(this.monster.scene);


        //Monster 2
        this.monster2 = this.monster.scene.clone();
        this.monster2.animations = this.monster.animations
        this.sceneGroup2.add(this.monster2);
        this.monster2.position.set(100, 15, -20);
        this.monster2.scale.set(15, 15, 15);
        this.monsterMixer2 = new THREE.AnimationMixer(this.monster2);

        this.monsters.push(this.monster);
        this.monsters.push(this.monster2);
        this.monstersLoaded = true;

        //Setup Listeners For Animations
        this.monsterMixer1.addEventListener('finished', function (e) {
            e.action.stop();
        })
        this.monsterMixer2.addEventListener('finished', function (e) {
            e.action.stop();
        })


        this.idleAnimations = this.monster.animations[1];
        this.idleFarAnimations = this.monster.animations[2];
        this.powerUpAnimations = this.monster.animations[3];
        this.shootAnimations = this.monster.animations[0];



        this.playAnimations(this.idleAnimations, this.monsterMixer1)
        this.playAnimations(this.idleAnimations, this.monsterMixer2)

        // console.log(this.monster.animations);
    }


    playAnimation(animation, mixer) {
        mixer.clipAction(animation).play()
    }
    playAnimations(animations, mixer) {
        for (let i = 0; i < animations.length; i++) {
            mixer.clipAction(animations[i]).play()
        }
    }
    playAnimationOnce(animation, mixer) {
        mixer.clipAction(animation).loop = THREE.LoopOnce;
        mixer.clipAction(animation).play()
    }

    playAnimationBack(animation, mixer) {
        mixer.clipAction(animation).timeScale = -1.5;
        mixer.clipAction(animation).play()

    }

    loopAnimation(animation, mixer) {
        mixer.clipAction(animation).loop()
    }

    stopAnimation(animation, mixer) {
        mixer.clipAction(animation).stop()
    }

    stopAnimationAll(mixer) {

        for (let i = 0; i < this.monster.animations.length; i++) {
            mixer.clipAction(this.monster.animations[i]).stop()
        }
    }



    loadBanana() {
        let model = ModelLoader.models.find(model => {
            return model.name === 'banana'
        })

        // let debris = ModelLoader.models.find(model => {
        //     return model.name === 'debris3'
        // })

        model.gltf.scene.traverse((node) => {
            if (node.isMesh && node.name === "Obj_000002") {
                this.banana = node;
                this.banana.morphTargets = true;
                // this.banana.material.flatShading = THREE.SmoothShading;

            }
        })
        let particleCount = 30;
        // now create the individual particles
        for (let p = 0; p < particleCount; p++) {

            // create a particle with random
            // position values, -250 -> 250
            let pX = -470 + p * 30,
                pY = 0,
                pZ = -100;

            let particle = this.banana.clone();
            particle.scale.set(0.5, 0.5, 0.5);
            particle.position.set(pX, pY, pZ);
            particle.userData.active = true;



            // add it to the geometry
            this.sceneGroup2.add(particle);
            this.bananaParticles.push(particle);

        }


        //Generate debris

        // this.reactingAnimation = debris.gltf.animations[0];

        // for (let i = 0; i < 30; i++) {
        //     let deb = debris.gltf.scene.clone();
        //     deb.children[0].scale.setScalar(0.05);
        //     let pX = -470 + i * 30,
        //         pY = 0,
        //         pZ = 0;
        //     deb.position.set(pX, pY, pZ);

        //     this.mixers.push(new THREE.AnimationMixer(deb))
        //     this.sceneGroup2.add(deb);
        //     this.debris.push(deb);

        // }

        // this.playAnimations(this.reactingAnimation);


    }

    playAnimations(animation) {
        for (let i = 0; i < this.mixers.length; i++) {
            this.mixers[i].clipAction(animation).play();
        }
    }

    update(time, delta, positions) {
        // if (this.hidden === false) {
            // time ;   


            if (this.monsters) {
                this.moveMonsters(positions);
            }

            this.animateDebris(time, delta);




            if (Math.round(this.internalTime) % 50 === 0) {
                this.checkHandConnectionObjects();
                this.measureDistanceBetween();
            }


            if (this.monsterMixer1 != null) {
                this.monsterMixer1.update(delta * 0.5);
            };


            if (this.monsterMixer2 != null) {
                this.monsterMixer2.update(delta * 0.5);

            };

            for (let i = 0; i < this.mixers.length; i++) {
                if (this.mixers[i]) {
                    this.mixers[i].update(delta);
                }
            }


        // }
    }

    updateTimeScale(animation, _timeScale) {
        for (let i = 0; i < this.mixers.length; i++) {
            this.mixers[i].clipAction(animation).timeScale = _timeScale;
        }

    }


    animateDebris(time, delta) {

        if (AudioManager.inited && AudioManager.midMeter) {

            // this.updateTimeScale(this.reactingAnimation, AudioManager.getAudioLevel());
            // for (let j = 0, l = this.debris.length; j < l; j++) {
            //     this.debris[j].position.y = Math.sin(time * 0.3 + j) * 250
            // }
        }

        for (let p = 0, l = this.bananaParticles.length; p < l; p++) {
            this.bananaParticles[p].position.y = Math.sin(time * 0.3 + p) * 250;
            this.bananaParticles[p].rotation.x = Math.sin(time * 0.3 + p) * 10;
        }


        // this.debrisGroup1.rotation.x += OSCManager.eq[0]
        // this.debrisGroup2.rotation.x += OSCManager.eq[0]

    }

    findClosestParticle(monsterPos) {
        let index = 0;
        let distance = Number.MAX_SAFE_INTEGER;
        if (this.bananaParticles) {
            for (let i = 0; i < this.bananaParticles.length; i++) {
                let dx = monsterPos.x - this.bananaParticles[i].position.x;
                let dy = monsterPos.y - this.bananaParticles[i].position.y;
                let dz = monsterPos.z - this.bananaParticles[i].position.z;

                let d = Math.sqrt(dx * dx + dy * dy + dz * dz);
                if (d < distance && this.bananaParticles[i].scale.x === 0.5) {
                    distance = d
                    index = i;
                }
            }
        }
        return index;
    }

    measureDistanceBetween() {
        let d = THREE.Math.mapLinear(this.monsters[0].scene.position.distanceTo(this.monsters[1].position), 0, 400, 400, 1);
        this.oscManager.sendMessage('/playerdistance', d);
        // console.log(d);
        if (d < 100 && this.powerUpState === false && this.farState === false) {
            // console.log("far");
            this.farState = true;
            this.stopAnimation(this.idleAnimations, this.monsterMixer1);
            this.stopAnimation(this.idleAnimations, this.monsterMixer2);
            this.playAnimation(this.idleFarAnimations, this.monsterMixer1);
            this.playAnimation(this.idleFarAnimations, this.monsterMixer2);

        } else if (d >= 100 && this.powerUpState === false) {
            this.stopAnimation(this.idleFarAnimations, this.monsterMixer1);
            this.stopAnimation(this.idleFarAnimations, this.monsterMixer2);
            this.playAnimation(this.idleAnimations, this.monsterMixer1);
            this.playAnimation(this.idleAnimations, this.monsterMixer2);


        }
    }

    clap() {

        if (this.state === 2) {



            SessionManager.setKeyInteraction(2);
            if (this.hasClapped === false) {
                this.hasClapped = true;

            }

            if (this.hasClapped === true) {

            }


            // if too far no shooty.
            // if (this.farState) {
            let index = this.findClosestParticle(this.monsters[0].scene.position);
            let index2 = this.findClosestParticle(this.monsters[1].position);

            this.score += 1;
            this.stopAnimationAll(this.monsterMixer1)
            this.stopAnimationAll(this.monsterMixer2)
            this.playAnimationOnce(this.shootAnimations, this.monsterMixer1);
            this.playAnimationOnce(this.shootAnimations, this.monsterMixer2);
            setTimeout(() => {
                this.playAnimation(this.idleAnimations, this.monsterMixer1)
                this.playAnimation(this.idleAnimations, this.monsterMixer2)

            }, 1000)

            this.handleHitBanana(this.bananaParticles[index])
            this.handleHitBanana(this.bananaParticles[index2])

            this.oscManager.sendMessage('/bananahit', THREE.Math.randInt(0, 3));
            this.createSprite(20, this.monsters[0].scene.position, this.bananaParticles[index].position);
            this.createSprite(20, this.monsters[1].position, this.bananaParticles[index2].position);

            // }


        }

    }

    handleHitBanana(banana, index) {
        TweenLite.to(banana.morphTargetInfluences, 0.25, {
            2: 1.0,
            onComplete: () => {
                TweenLite.to(banana.scale, 1.0, {
                    x: 0.001,
                    y: 0.001,
                    z: 0.001,

                    onComplete: () => {
                        let color = Palette[this.colorCounter % 8];
                        // background color
                        let container = document.getElementById("container");
                        container.style.background = 'linear-gradient(to bottom' + ', ' + color.top + ', ' + color.bottom + ')';
                        if (this.colorCounter < Palette.length) {
                            this.colorCounter += 1;
                        } else {
                            this.colorCounter = 0;
                        }

                        banana.userData.active = false;
                        setTimeout(() => {
                            // this.playAnimation(this.idleAnimations, this.monsterMixer1)
                            // this.playAnimation(this.idleAnimations, this.monsterMixer2)
                            // this.stopAnimation(this.shootAnimations, this.monsterMixer1)
                            // this.stopAnimation(this.shootAnimations, this.monsterMixer2)
                        }, 500)


                        setTimeout(() => {
                            banana.userData.active = true;

                            TweenLite.to(banana.scale, 0.25, {
                                x: 0.5,
                                y: 0.5,
                                z: 0.5,
                                onComplete: () => {
                                    TweenLite.to(banana.morphTargetInfluences, 0.1, {
                                        2: 0
                                    })
                                }
                            });
                        }, 25000)
                    }
                });
            }
        })
    }

    show(camera) {
        ////console.log("scene 2 show sdf");
        this.hidden = false;
        this.score = 0;
        camera.position.set(0, 0, 200);
        camera.lookAt(0, 0, 0);
        this.sceneGroup2.visible = true;

        // TweenLite.to(this.sceneGroup2.position, 0.1, {
        //     y: 0,
        // })
        this.sceneGroup2.position.y = 0;

        document.getElementById('video_bg1').style.display = "none";
        document.getElementById('video_bg3').style.display = "none";

    }

    hide(creature) {
        ////console.log("scene 2 hide");
        this.hasClapped = false;
        // document.getElementById("circle").style.display = "none";
        // TweenLite.to(this.sceneGroup2.position, 1.0, {
        //     y: -1000,
        //     onComplete: () => {
        //         this.sceneGroup2.visible = false;
        //     }
        // })

        this.sceneGroup2.visible = false;


    }

    setState(state, camera) {
        this.state = state;
        this.show(camera);

        if (state === 2) {
        } else {
            // this.hide();
        }
    }

    moveMonsters(positions) {

        if (positions[0]) {
            // //console.log(positions);
            let newPosX = THREE.Math.mapLinear(positions[0].x, 1280, 0, -230, 230);
            let newPosY = THREE.Math.mapLinear(positions[0].y, 0, 720, 160, -160);
            let newVect = new THREE.Vector3(newPosX, newPosY, this.monsters[0].scene.position.z);

            let currentX = this.monsters[0].scene.position.x

            // Calculate the distance from this new point to old to see if its the right creature
            let distance = this.monsters[0].scene.position.distanceTo(newVect);
            let distance2 = this.monsters[1].position.distanceTo(newVect);

            let leftWristX = THREE.Math.mapLinear(positions[0].leftWrist.x, 1280, 0, -230, 230)
            let leftWristY = THREE.Math.mapLinear(positions[0].leftWrist.y, 1280, 0, -160, 160)
            let rightWristX = THREE.Math.mapLinear(positions[0].rightWrist.x, 1280, 0, -230, 230)
            let rightWristY = THREE.Math.mapLinear(positions[0].rightWrist.y, 1280, 0, -160, 160)
            let rightWrist = new THREE.Vector3(rightWristX, rightWristY, 0);
            let leftWrist = new THREE.Vector3(leftWristX, leftWristY, 0);


            // this.checkIfLeftOrRight(currentX, newVect.x, this.monsters[0].scene, this.monsterPosState)

            // So only if the new postion from webcam is closer to the monster
            if (distance <= distance2) {
                this.checkIfLeftOrRight(currentX, newVect.x, this.monsters[0].scene, this.monsterPosState)

                this.monsters[0].scene.position.lerp(newVect, 0.2);
                this.handSpheres[0].position.lerp(leftWrist, 0.1);
                this.handSpheres[1].position.lerp(rightWrist, 0.1);

            } else {
                this.monsters[1].position.lerp(newVect, 0.2);
                this.checkIfLeftOrRight(currentX, newVect.x, this.monsters[1], this.monster2PosState)
                this.handSpheres[2].position.lerp(leftWrist, 0.1);
                this.handSpheres[3].position.lerp(rightWrist, 0.1);
            }



        }

        if (positions[1]) {
            let newPosX = THREE.Math.mapLinear(positions[1].x, 1280, 0, -230, 230);
            let newPosY = THREE.Math.mapLinear(positions[1].y, 0, 720, 160, -160);
            let newVect = new THREE.Vector3(newPosX, newPosY, this.monsters[1].position.z);

            let currentX = this.monsters[0].scene.position.x

            let leftWristX = THREE.Math.mapLinear(positions[1].leftWrist.x, 1280, 0, -230, 230)
            let leftWristY = THREE.Math.mapLinear(positions[1].leftWrist.y, 1280, 0, -160, 160)
            let rightWristX = THREE.Math.mapLinear(positions[1].rightWrist.x, 1280, 0, -230, 230)
            let rightWristY = THREE.Math.mapLinear(positions[1].rightWrist.y, 1280, 0, -160, 160)
            let rightWrist = new THREE.Vector3(rightWristX, rightWristY, 0);
            let leftWrist = new THREE.Vector3(leftWristX, leftWristY, 0);



            // Calculate the distance from this new point to old to see if its the right creature
            let distance = this.monsters[1].position.distanceTo(newVect);
            let distance2 = this.monsters[0].scene.position.distanceTo(newVect);
            // this.checkIfLeftOrRight(currentX, newVect.x, this.monsters[1]);
            // this.checkIfLeftOrRight(currentX, newVect.x, this.monsters[1], this.monster2PosState)

            // So only if the new postion from webcam is closer to the monster
            if (distance < distance2) {
                this.monsters[1].position.lerp(newVect, 0.2);
                this.checkIfLeftOrRight(currentX, newVect.x, this.monsters[1], this.monster2PosState)
                this.handSpheres[2].position.lerp(leftWrist, 0.1);
                this.handSpheres[3].position.lerp(rightWrist, 0.1);

            } else {
                this.monsters[0].scene.position.lerp(newVect, 0.2);
                this.checkIfLeftOrRight(currentX, newVect.x, this.monsters[0].scene, this.monsterPosState)
                this.handSpheres[0].position.lerp(leftWrist, 0.1);
                this.handSpheres[1].position.lerp(rightWrist, 0.1);
            }
        }
    }

    checkIfLeftOrRight(currentX, newX, monsterScene, state) {
        let mesh = monsterScene.children[1]
        // console.log(monsterScene.children);
        mesh.material.morphTargets = true;
        // // console.log(currentX- newX);
        let newDifference = THREE.Math.mapLinear(Math.abs(currentX - newX), 10, 30, -1, 1)
        this.difference = THREE.Math.lerp(this.difference, newDifference, 0.05);
        this.difference = THREE.Math.clamp(this.difference, -0.5, 0.5);
        // this.monsterScene.rotation.y = this.difference;

        if ((currentX - newX) > 10) {
            // if(state != -1) {
            //     console.log("LEFT");

            //     state = -1;
            //     monsterScene.children[2].position.x = -3;
            //     monsterScene.children[3].position.x = -3;
            //     monsterScene.children[4].position.x = -3;
            // }
            // monsterScene.scale.x = this.difference


        } else if ((currentX - newX) <= -10) {
            // if(state != 1) {
            //     console.log("Right");

            //     state = 1;
            //     monsterScene.children[2].position.x = 3;
            //     monsterScene.children[3].position.x = 3;
            //     monsterScene.children[4].position.x = 3;
            // }
            // monsterScene.scale.x = this.difference

        } else {
            // monsterScene.scale.x = 15;

        }

    }

    checkHandConnectionObjects() {

        // Self love


        let l1l2 = this.handSpheres[0].position.distanceToSquared(this.handSpheres[2].position);
        if (l1l2 < 400) {
            this.handSpheres[0].material.color.setHex(0xff00ff);
            this.handSpheres[2].material.color.setHex(0xff00ff);
            this.powerUp()

        } else {
            this.handSpheres[0].material.color.setHex(0xfffa00);
            this.handSpheres[2].material.color.setHex(0xfffa00);
            this.powerDown()

        }

        let r1l2 = this.handSpheres[1].position.distanceToSquared(this.handSpheres[2].position);
        if (r1l2 < 500) {
            this.handSpheres[1].material.color.setHex(0xffffff);
            this.handSpheres[2].material.color.setHex(0xffffff);
            this.powerUp()

        } else {
            this.handSpheres[1].material.color.setHex(0xfffa00);
            this.handSpheres[2].material.color.setHex(0xfffa00);
            this.powerDown()

        }
        let l1r2 = this.handSpheres[0].position.distanceToSquared(this.handSpheres[3].position);
        if (l1r2 < 500) {
            this.handSpheres[0].material.color.setHex(0x00ffff);
            this.handSpheres[3].material.color.setHex(0x00ffff);
            this.powerUp()

        } else {
            this.handSpheres[0].material.color.setHex(0xfffa00);
            this.handSpheres[3].material.color.setHex(0xfffa00);
            this.powerDown()

        }

        let r1r2 = this.handSpheres[1].position.distanceToSquared(this.handSpheres[3].position);
        if (r1r2 < 500) {
            this.handSpheres[1].material.color.setHex(0x00ff00);
            this.handSpheres[3].material.color.setHex(0x00ff00);
            this.powerUp()

        } else {
            this.handSpheres[1].material.color.setHex(0xfffa00);
            this.handSpheres[3].material.color.setHex(0xfffa00);
            this.powerDown()

        }



    }

    selfLove() {

        let selfd = this.handSpheres[0].position.distanceToSquared(this.handSpheres[1].position);
        if (selfd < 800) {
            this.handSpheres[0].material.color.setHex(0xff0000);
            this.handSpheres[1].material.color.setHex(0xff0000);
            this.powerUp()
        } else {
            this.handSpheres[0].material.color.setHex(0xfffa00);
            this.handSpheres[1].material.color.setHex(0xfffa00);
            this.powerDown()

        }
    }

    powerUp() {

        this.powerUpState = true;

        // Activate animations
        // this.playAnimation()
        this.stopAnimation(this.idleAnimations, this.monsterMixer1);
        this.stopAnimation(this.idleFarAnimations, this.monsterMixer1);
        this.stopAnimation(this.shootAnimations, this.monsterMixer1);
        this.stopAnimation(this.idleAnimations, this.monsterMixer2);
        this.stopAnimation(this.idleFarAnimations, this.monsterMixer2);
        this.stopAnimation(this.shootAnimations, this.monsterMixer2);

        this.playAnimation(this.powerUpAnimations, this.monsterMixer1);
        this.playAnimation(this.powerUpAnimations, this.monsterMixer2);

        //State to stop over playing.
        if (this.powerUpPlayState === false) {


            // Grow Monster
            TweenLite.to(this.monsters[0].scene.scale, 2, {
                x: 22,
                y: 22,
                z: 22,
                onComplete: () => {
                    this.powerUpPlayState = true;

                    // Set Timeout
                    setTimeout(() => {
                        TweenLite.to(this.monsters[0].scene.scale, 2, {
                            x: 15,
                            y: 15,
                            z: 15,
                            onComplete: () => {
                                this.powerUpPlayState = false;
                                this.powerUpState = false;
                                this.stopAnimation(this.powerUpAnimations, this.monsterMixer1);
                                this.stopAnimation(this.powerUpAnimations, this.monsterMixer2);
                                this.playAnimation(this.idleAnimations, this.monsterMixer1);
                                this.playAnimation(this.idleAnimations, this.monsterMixer2);
                            }
                        })
                    }, 3000)
                }
            })

            TweenLite.to(this.monsters[1].scale, 2, {
                x: 22,
                y: 22,
                z: 22,
                onComplete: () => {
                    setTimeout(() => {
                        TweenLite.to(this.monsters[1].scale, 2, {
                            x: 15,
                            y: 15,
                            z: 15,
                            onComplete: () => {}
                        })
                    }, 5000)
                }
            })
        }


    }

    powerDown() {
        // this.powerUpState = false;

        // // Deactivate animations
        // this.powerUpMonster1.stop();
        // this.powerUpMonster2.stop();

        // if (this.powerUpPlayState === true) {
        //     this.powerUpPlayState = false;
        // }

    }



    createSprite(size, startPos, endPos) {
        var spriteMaterial = new THREE.SpriteMaterial({
            map: this.sprite,
            blending: THREE.AdditiveBlending,
            depthTest: false,
            transparent: false,
        });


        var sprite = new THREE.Sprite(spriteMaterial);
        sprite.scale.set(size, size, size);
        sprite.position.set(startPos.x, startPos.y, 0);

        this.sceneGroup2.add(sprite);

        TweenLite.to(sprite.position, 0.3, {
            x: endPos.x,
            y: endPos.y,
            z: endPos.z + 20,
            onComplete: () => {
                this.sceneGroup2.remove(sprite);
            }
        })
    }

}