export default [

        {
            top: '#F8EC08',
            bottom: '#EBFF49'
        },
        {
            top: '#00ACC8',
            bottom: '#EBFF49'
        },
        {
            top: '#1538FD',
            bottom: '#007D56'
        },
        {
            top: '#1538FD',
            bottom: '#00ACC8'
        },
        {
            top: '#1538FD',
            bottom: '#EBFF49'
        },
        {
            top: '#F3F3F3',
            bottom: '#FFCEDC'
        },
        {
            top: '#1538FD',
            bottom: '#F3332A'
        },
        {
            top: '#EBFF49',
            bottom: '#F3F3F3'
        },
        {
            top: '#007D56',
            bottom: '#00ACC8'
        },


]
