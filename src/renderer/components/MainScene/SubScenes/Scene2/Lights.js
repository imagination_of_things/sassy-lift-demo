import * as THREE from 'three'
import {
    TweenLite
} from 'gsap'


export default class Lights {



    constructor(sceneGroup) {
        this.sceneGroup = sceneGroup;
        this.initLights();

    }


    initLights() {
        this.lightGroup1 = new THREE.Group();
        this.lightGroup2 = new THREE.Group();
        this.lightGroup3 = new THREE.Group();

        this.lightGroup1.position.set(0, 0, 0);
        this.lightGroup2.position.set(0, 0, 0);
        this.lightGroup3.position.set(0, 0, 0);


        let light = new THREE.PointLight("#14efff", 1.61);
        light.position.y = 30;
        light.position.x = -150;
        light.position.z = 70;
        this.lightGroup1.add(light);

        let light2 = new THREE.SpotLight("#24c43c", 0.76);
        light2.position.y = 30;
        light2.position.x = 150;
        light2.position.z = 70;
        this.lightGroup1.add(light2);

        let light3 = new THREE.PointLight("#01ff1c", 0.2);
        light3.position.y = 60;
        light3.position.x = 90;
        light3.position.z = 2;
        this.lightGroup1.add(light3);

        let light4 = new THREE.SpotLight("#01ff1c", 0.2);
        light4.position.y = 60;
        light4.position.x = -90;
        light4.position.z = 3;
        this.lightGroup1.add(light4);

        // -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

        let light5 = new THREE.PointLight("#1071ff", 1.61);
        light5.position.y = 30;
        light5.position.x = -150;
        light5.position.z = 70;
        this.lightGroup2.add(light5);

        let light6 = new THREE.SpotLight("#ff005d", 0.76);
        light6.position.y = 30;
        light6.position.x = 150;
        light6.position.z = 70;
        this.lightGroup2.add(light6);

        let light7 = new THREE.PointLight("#1071ff", 0.2);
        light7.position.y = 60;
        light7.position.x = 90;
        light7.position.z = 20;
        this.lightGroup2.add(light7);

        let light8 = new THREE.SpotLight("#fff4f9", 0.2);
        light8.position.y = 60;
        light8.position.x = -90;
        light8.position.z = -40;
        this.lightGroup2.add(light8);

        // -=-=-=-=-=-=-=-=-=-=-=-==-=-=--=-=-=--==-=-=-=-=-=-=-=--==-=-=-=-=-=-=-=-=-=--==-=-=-=-=-=-=--==-

        let light9 = new THREE.PointLight("#ff041b", 1.2);
        light9.position.y = -12;
        light9.position.x = -160;
        light9.position.z = 70;
        this.lightGroup3.add(light9);

        let light10 = new THREE.SpotLight("#ff041b", 1.2);
        light10.position.y = 12;
        light10.position.x = 160;
        light10.position.z = 70;
        this.lightGroup3.add(light10);

        let light11 = new THREE.PointLight("#ffcaa7", 0.9);
        light11.position.y = 50;
        light11.position.x = 50;
        light11.position.z = 100;
        this.lightGroup3.add(light11);

        let light12 = new THREE.SpotLight("#ffcaa7", 0.4);
        light12.position.y = 50;
        light12.position.x = -50;
        light12.position.z = 100;
        this.lightGroup3.add(light12);




        this.sceneGroup.add(this.lightGroup1);
        this.sceneGroup.add(this.lightGroup2);
        this.sceneGroup.add(this.lightGroup3);





    }


    setLightMode1() {

        
        this.lightGroup1.visible = false;
        this.lightGroup2.visible = true;
        this.lightGroup3.visible = false;
        



    }

    setLightMode2() {
        this.lightGroup1.visible = true;
        this.lightGroup2.visible = false;
        this.lightGroup3.visible = false;
        


    }

    setLightMode3() {
        this.lightGroup1.visible = false;
        this.lightGroup2.visible = false;
        this.lightGroup3.visible = true;
        


    }


    update(time, positions) {

    }



    reset() {

    }


    show() {
        this.hidden = false;


    }



    hide() {

    }

    setGameState(state) {
        this.state = state;
        console.log("gameState", state)

        if (this.state === 0) {
            this.setLightMode1()
        } else if (this.state === 1) {
            this.setLightMode2()

        } else if (this.state === 2) {
            this.setLightMode3()

        }
    }
}