import * as THREE from 'three'
import {
    TweenLite
} from 'gsap'
import Simple1DNoise from '../util/Perlin'
import AudioManager from '../../AudioManager/AudioManager'

import Palette from './Colors'



export default class MultiCreatures {



    constructor(creature) {

        //create custom event
        if (typeof document.CustomEvent === 'function') {
            this.event = new document.CustomEvent('gameState', {
                bubbles: true,
                cancelable: true
            });
        } else if (typeof document.createEvent === 'function') {
            this.event = document.createEvent('Event');
            this.event.initEvent('gameState', true, true);
        } else {
            return false;
        }

        if (typeof document.CustomEvent === 'function') {
            this.scoreEvent = new document.CustomEvent('score', {
                bubbles: true,
                cancelable: true
            });
        } else if (typeof document.createEvent === 'function') {
            this.scoreEvent = document.createEvent('Event');
            this.scoreEvent.initEvent('score', true, true);
        } else {
            return false;
        }



        this.sceneGroup = new THREE.Group();
        this.baseCreature = creature;
        this.creatures = [];

        this.simpleAudioLevel = 0.0;

        this.whenExplodes = [150, 200, 250, 300, 320, 220];
        this.thresholds = [0, 0, 0, 0, 0, 0];

        this.spins = [0.05, -0.1, -0.25, 0.25, -0.4, 0.4];
        this.ringExplosions = this.setRingExplosions();
        this.explosionState = [false, false, false, false, false, false];


        this.score = 0;
        this.lastTime = new Date();

        this.hasProgressed = false;


        this.generator = new Simple1DNoise();


        for (let i = 0; i < 6; i++) {
     
                this.creatures.push(this.baseCreature.clone());
                this.creatures[i].position.x = 400 * Math.sin(2 * Math.PI / (6) * i)
                this.creatures[i].position.y = 200 * Math.cos(2 * Math.PI / (6) * i);
                this.creatures[i].scale.setScalar(10)
            
      
        
            this.sceneGroup.add(this.creatures[i])
        }
        this.sceneGroup.position.z = -1000;

        this.hide();
    }


    resetThresholds() {
        this.thresholds = [0, 0, 0, 0, 0, 0]
    }

    demoExplode() {

                //This handles the fruit destoying
                let currentTime = new Date();
                let timeDifference = currentTime.getTime() - this.lastTime.getTime();
                if (timeDifference > 1000) {
                    console.log(this.score)
                    this.creatures[this.score].visible = false;
                    this.explosionState[this.score] = true;

                    window.dispatchEvent(this.scoreEvent);
                    this.lastTime = new Date();
                    //Explosion ring

        
                    TweenLite.to(this.ringExplosions[this.score].scale, 0.8, {
                        x: 800,
                        y: 800,
                        onComplete: () => {
                            this.ringExplosions[this.score].visible = false;
                            this.score += 1

                            // if(this.score === 6) {
                            //     this.reset();

                            //     // Dispatch event for gameChange
                            //     window.dispatchEvent(this.event);

                            //     // this.resetThresholds
                            // }

                        }
                    })

                }


    }




    update(time, positions) {

        this.simpleAudioLevel = AudioManager.getSimplerAudioLevel();

        for (let i = 0; i < 6; i++) {
            this.creatures[i].position.x = (this.generator.getVal(time) * 0.5 + 0.5) * 500 * Math.sin((2 * Math.PI * i) / (6) + time * 0.3)
            this.creatures[i].position.y = (this.generator.getVal(time) * 0.5 + 0.5) * 200 * Math.cos((2 * Math.PI * i) / (6) + time * 0.3);

            if (this.simpleAudioLevel > 0.8) {
                this.thresholds[i] += 1
                this.creatures[i].rotation.y += this.spins[i] + (this.spins[i]*0.5*this.simpleAudioLevel);
            }

            // If we have held above threshold for this amount and hasn't exploded
            if (this.thresholds[i] > this.whenExplodes[i] && this.explosionState[i] === false) {

                //hide creature

                //This handles the fruit destoying
                let currentTime = new Date();
                let timeDifference = currentTime.getTime() - this.lastTime.getTime();
                if (timeDifference > 1000) {
                    //Explosion ring
                    this.creatures[i].visible = false;
                    this.explosionState[i] = true;

                    window.dispatchEvent(this.scoreEvent);
                    this.lastTime = new Date();
                    this.score += 1

                    TweenLite.to(this.ringExplosions[i].scale, 0.8, {
                        x: 800,
                        y: 800,
                        onComplete: () => {
                            this.ringExplosions[i].visible = false;

                            // if(this.score === 6) {
                            //     this.reset();

                            //     // Dispatch event for gameChange
                            //     window.dispatchEvent(this.event);

                            //     // this.resetThresholds
                            // }

                        }
                    })

                }






            }
            if (!this.explosionState[i]) {
                // this.creatures[i].scale.setScalar(3 + 40*this.simpleAudioLevel)
                this.ringExplosions[i].position.x = (this.generator.getVal(time) * 0.5 + 0.5) * 400 * Math.sin((2 * Math.PI * i) / (6) + time * 0.3)
                this.ringExplosions[i].position.y = (this.generator.getVal(time) * 0.5 + 0.5) * 200 * Math.cos((2 * Math.PI * i) / (6) + time * 0.3);
            }

        }





    }


    setRingExplosions() {

        let array = []

        for (let i = 0; i < 6; i++) {
            var geometry = new THREE.RingGeometry(1, 1.01, 32);
            var material = new THREE.MeshBasicMaterial({
                color: Palette[i].top,
                side: THREE.DoubleSide
            });
            var mesh = new THREE.Mesh(geometry, material);
            array.push(mesh);
            this.sceneGroup.add(mesh);
        }
        return array;
    }


    reset() {

        // Reset thresholds
        this.resetThresholds();

            this.creatures[0].visible = true;
            this.creatures[1].visible = true;
            this.creatures[2].visible = true;
            this.creatures[3].visible = true;
            this.creatures[4].visible = true;
            this.creatures[5].visible = true;



        // Reset rings

        for (let i = 0; i < 6; i++) {
            // reset rings
            this.ringExplosions[i].scale.x = 1;
            this.ringExplosions[i].scale.y = 1;

            this.ringExplosions[i].visible = true;
            this.explosionState[i] = false;




        }

        this.score = 0;

    }


    show() {

        TweenLite.to(this.sceneGroup.position, 1, {
            z: 0
        })
    }



    hide() {
        TweenLite.to(this.sceneGroup.position, 1, {
            z: -1000
        })
    }

    setGameState(state) {

    }
}