import * as THREE from 'three'
import FastSimplexNoise from '../util/Fast-Simplex-Noise'
import ModelLoader from '../../SceneManager/ModelLoader'

import {
    TweenLite,
    TweenMax
} from 'gsap'

import Boid from './boidbird'
import Bird from './bird'
const TREE_NUM = 9;

import OSCManager from '../../OSCManager/OSCManager'
import AudioManager from '../../AudioManager/AudioManager'

import Perlin from '../util/Perlin'
const color = {
    top: '#007D56',
    bottom: '#00ACC8'
}

import Palette from '../Scene2/Colors'
import SessionManager from '../../SessionManager/SessionManager';

const bassIndexes = [0, 3, 6];
const midIndexes = [1, 4, 7];
const trebleIndexes = [2, 5, 8];

export default class Scene3 {
    constructor(renderer, scene, size) {
        this.eq = [];
        this.colorCounter = 0;

        this.shouldUpdate = false;
        this.listening = true;
        this.generator = new Perlin();
        this.wiggleRate = 10;
        this.sessionTime = performance.now();

        this.audioStatus = 0;

        this.sceneGroup = new THREE.Group();
        scene.add(this.sceneGroup);
        this.sceneGroup.position.set(0, 0, -2500);
        this.textureLoader = new THREE.TextureLoader();
        this.renderer = renderer;
        this.trees = [];
        this.mixers = []

        this.loadAnimationGLTF();
        this.init();
        this.container = document.getElementById('container');
        // this.testSpheres = [];
        // this.backgroundGroup = new THREE.Group();
        // scene.add(this.backgroundGroup);


    }

    loadTestSpheres() {
        var geometry = new THREE.PlaneBufferGeometry(10, 10, 10, 10);
        var material = new THREE.MeshStandardMaterial({
            color: 0xffff00
        });
        var sphere = new THREE.Mesh(geometry, material);
        sphere.position.set(10, 0, 50);
        this.backgroundGroup.add(sphere);
        let sphere2 = sphere.clone();
        this.backgroundGroup.add(sphere2);

        sphere2.position.set(15, 0, 50);
        let sphere3 = sphere.clone();
        sphere3.position.set(5, 0, 50);
        this.backgroundGroup.add(sphere3);



        this.testSpheres.push(sphere)
        this.testSpheres.push(sphere2)
        this.testSpheres.push(sphere3)

    }



    loadAnimationGLTF() {
        this.mid = ModelLoader.models.find(model => {
            return model.name === 'mid'
        })

        this.treble = ModelLoader.models.find(model => {
            return model.name === 'treble'
        })

        this.bass = ModelLoader.models.find(model => {
            return model.name === 'bass'
        })





        // Place Trees

        this.placeTree(this.bass.gltf, 0);
        this.placeTree(this.mid.gltf, 1);
        this.placeTree(this.treble.gltf, 2);
        this.placeTree(this.bass.gltf, 3);
        this.placeTree(this.mid.gltf, 4);
        this.placeTree(this.treble.gltf, 5);
        this.placeTree(this.bass.gltf, 6);
        this.placeTree(this.mid.gltf, 7);
        this.placeTree(this.treble.gltf, 8);


        // Get animations
        //LISTENING

        this.listeningAnimations = [
            this.bass.gltf.animations[3],
            this.mid.gltf.animations[3],
            this.treble.gltf.animations[1],
        ]

        this.reactingAnimations = [
            this.bass.gltf.animations[2],
            this.mid.gltf.animations[2],
            this.treble.gltf.animations[2],
        ]


        this.playAnimation(bassIndexes, this.listeningAnimations[0])
        this.playAnimation(midIndexes, this.listeningAnimations[1])
        this.playAnimation(trebleIndexes, this.listeningAnimations[2])



    }

    placeTree(treeGLTF, i) {

        let tree = treeGLTF.scene.clone();
        tree.animations = treeGLTF.animations;
        tree.position.set(50 * Math.sin(2 * Math.PI * (i / TREE_NUM)), 0, 50 * Math.cos(2 * Math.PI * (i / TREE_NUM)));
        this.sceneGroup.add(tree);
        if (this.trees.length < TREE_NUM) {
            this.trees.push(tree);
            this.mixers.push(new THREE.AnimationMixer(tree))
        }
        tree.children[0].material.morphTargets = true;
        tree.scale.set(5, 7, 5);


    }

    playAnimation(indexes, animation) {
        for (let i = 0; i < indexes.length; i++) {
            this.mixers[indexes[i]].clipAction(animation).timeScale = 0.35;
            this.mixers[indexes[i]].clipAction(animation).play();
        }
    }

    stopAnimation(indexes, animation) {
        for (let i = 0; i < indexes.length; i++) {
            this.mixers[indexes[i]].clipAction(animation).stop();
        }
    }

    updateTimeScale(indexes, animation, _timeScale) {
        for (let i = 0; i < indexes.length; i++) {
            this.mixers[indexes[i]].clipAction(animation).timeScale = _timeScale;
        }
    }



    init() {
        this.boid = new Boid();
        this.boid.init(this.renderer, this.sceneGroup);
    }



    audioStatus(value) {
        ////console.log("AudioStatus,", value);
    }

    update(time, camera, delta, positions) {

        if (this.hidden === false) {
            this.boid.update();
            this.sceneGroup.rotation.y += 0.002;
            this.checkAudioStatus();

            if (this.trees.length === TREE_NUM) {
                this.animateReacting();
            }




            // this.sceneGroup.position.z = Math.sin(time * 0.25) * 10;

            for (let i = 0; i < TREE_NUM; i++) {
                if (this.mixers[i]) {
                    this.mixers[i].update(delta);
                }
            }
        }
    }

    animateReacting() {
        if (AudioManager.inited) {
            // this.eq = AudioManager.getBassMidTreble();
            this.updateTimeScale(bassIndexes, this.reactingAnimations[0], AudioManager.getBassMidTreble()[0]*2)
            this.updateTimeScale(midIndexes, this.reactingAnimations[1], AudioManager.getBassMidTreble()[1]*2)
            this.updateTimeScale(trebleIndexes, this.reactingAnimations[2], AudioManager.getBassMidTreble()[2]*3)

            this.updateTimeScale(bassIndexes, this.listeningAnimations[0], AudioManager.getBassMidTreble()[1]*2)
            this.updateTimeScale(midIndexes, this.listeningAnimations[1], AudioManager.getBassMidTreble()[1]*2)
            this.updateTimeScale(trebleIndexes, this.listeningAnimations[2], AudioManager.getBassMidTreble()[1]*3)
        }

    }

    clap() {

        let color = Palette[this.colorCounter % 8];
        // background color
        let container = document.getElementById("container");
        container.style.background = 'linear-gradient(to bottom' + ', ' + color.top + ', ' + color.bottom + ')';

        // grow organisms slightly
        this.growOrganisms();

        if(this.colorCounter < Palette.length){
            this.colorCounter+=1;
        } else {
            this.colorCounter = 0;
        }
    }

    growOrganisms() {
        for(let i=0, l=this.trees.length; i<l; i++) {
            TweenLite.to(this.trees[i].scale, 0.5, {
                x: 5 + 5,
                y: 7 + 5*2,
                z: 5 + 5,
                onComplete: () => {
                    TweenLite.to(this.trees[i].scale, 0.5, {
                        x: 5,
                        y: 7,
                        z: 5,
                    })

                }
            })
        }
 
    }

    checkAudioStatus() {
        if (OSCManager.audioStatus != this.audioStatus) {
            this.audioStatus = OSCManager.audioStatus;
            // console.log(this.audioStatus);
            if (this.audioStatus === 1) {
                this.listening = true;

                // STOP REACTING
                this.stopAnimation(bassIndexes, this.reactingAnimations[0])
                this.stopAnimation(midIndexes, this.reactingAnimations[1])
                this.stopAnimation(trebleIndexes, this.reactingAnimations[2])

                //LISTENING
                this.playAnimation(bassIndexes, this.listeningAnimations[0])
                this.playAnimation(midIndexes, this.listeningAnimations[1])
                this.playAnimation(trebleIndexes, this.listeningAnimations[2])


            } else {
                this.listening = false;
                ////console.log("not listening");
                // this.stopListeningSilence();

                // STOP REACTING
                this.stopAnimation(bassIndexes, this.listeningAnimations[0])
                this.stopAnimation(midIndexes, this.listeningAnimations[1])
                this.stopAnimation(trebleIndexes, this.listeningAnimations[2])

                //LISTENING
                this.playAnimation(bassIndexes, this.reactingAnimations[0])
                this.playAnimation(midIndexes, this.reactingAnimations[1])
                this.playAnimation(trebleIndexes, this.reactingAnimations[2])

            }

        }
    }




    show(camera) {
        let color = Palette[this.colorCounter % 8];
        // background color
        let container = document.getElementById("container");
        container.style.background = 'linear-gradient(to bottom' + ', ' + color.top + ', ' + color.bottom + ')';
        camera.position.set(0, 0, 0);
        camera.lookAt(1, 0, 5);
        // came
        this.hidden = false;
        this.colorCounter = 0;
        this.growOrganisms();
        TweenLite.to(this.sceneGroup.position, 0.5, {
            z: 0,
        })
    }

    hide() {
        // document.getElementById('video_bg3').style.display = "none";

        this.hidden = true;
        ////console.log("scene 3 hide");
        TweenLite.to(this.sceneGroup.position, 0.5, {
            z: -2500,
            onComplete: () => {

            }
        })

    }


    setState(state, camera) {

        if (state === 3) {
            this.show(camera);
            this.shouldUpdate = true;

        } else {
            this.hide();
            this.shouldUpdate = false;

            // this.hide(creature2);


        }
    }
}