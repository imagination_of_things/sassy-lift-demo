import * as THREE from 'three'
import GPUComputationRenderer from './GPUCompute'
import Bird from './bird'

export default function Boid() {

    var BOUNDS = 800, BOUNDS_HALF = BOUNDS / 2;


    var hash = document.location.hash.substr(1);
    if (hash) hash = parseInt(hash, 0);


    let parameters = [
        [
            [0.95, 0, 0.5]
        ],
        [
            [0.95, 0, 0.5]
        ],
        [
            [0.80, 0, 0.5]
        ],
        // [[ 0.85, 0, 0.5 ]],
        [
            [0.80, 0, 0.5]
        ]
    ];

    var WIDTH = 32;

    function change(n) {
        location.hash = n;
        location.reload();
        return false;
    }

    this.last = performance.now();


    this.init = (renderer, sceneGroup) => {
        this.initComputeRenderer(renderer);

        // this.last = performance.now();
        this.gpuCompute;
        this.velocityVariable;
        this.positionVariable;
        this.positionUniforms;
        this.velocityUniforms;
        this.birdUniforms;
        this.sceneGroup = sceneGroup;
        this.initBirds();
    }
    this.initBirds = ()=> {
        var geometry = new Bird();
        // For Vertex and Fragment
        this.birdUniforms = {
            color: {
                value: new THREE.Color(0xffff00)
            },
            texturePosition: {
                value: null
            },
         
            textureVelocity: {
                value: null
            },
            time: {
                value: 2.0
            },
            delta: {
                value: 0.0
            }
        };
        // ShaderMaterial
        var material = new THREE.ShaderMaterial({
            uniforms: this.birdUniforms,
            vertexShader: document.getElementById('birdVS').textContent,
            fragmentShader: document.getElementById('birdFS').textContent,
            side: THREE.DoubleSide
        });
        var birdMesh = new THREE.Mesh(geometry, material);
        // birdMesh.rotation.y = Math.PI / 2;
        birdMesh.matrixAutoUpdate = false;
        birdMesh.updateMatrix();
        birdMesh.position.set(0,0,0);
        // birdMesh.scale.set(0.25,0.25,0.25);
        birdMesh.rotation.x = Math.PI/2;
        this.sceneGroup.add(birdMesh);
    }

    this.fillPositionTexture =(texture) =>{
        var theArray = texture.image.data;
        for (var k = 0, kl = theArray.length; k < kl; k += 4) {
            var x = 0 + Math.random() * BOUNDS - BOUNDS_HALF;
            var y = Math.random() * BOUNDS - BOUNDS_HALF;
            var z = 0+ Math.random() * BOUNDS - BOUNDS_HALF;
            theArray[k + 0] = x;
            theArray[k + 1] = y;
            theArray[k + 2] = z;
            theArray[k + 3] = 1;
        }
    }

    this.fillVelocityTexture=(texture)=> {
        var theArray = texture.image.data;
        for (var k = 0, kl = theArray.length; k < kl; k += 4) {
            var x = Math.random() - 0.5;
            var y = Math.random() - 0.5;
            var z = Math.random() - 0.5;
            theArray[k + 0] = x * 10;
            theArray[k + 1] = y * 10;
            theArray[k + 2] = z * 10;
            theArray[k + 3] = 1;
        }
    }



   this.initComputeRenderer =(renderer)=> {

        this.gpuCompute = new GPUComputationRenderer(WIDTH, WIDTH, renderer);
        var dtPosition = this.gpuCompute.createTexture();
        var dtVelocity = this.gpuCompute.createTexture();
        this.fillPositionTexture(dtPosition);
        this.fillVelocityTexture(dtVelocity);
        this.velocityVariable = this.gpuCompute.addVariable("textureVelocity", document.getElementById('fragmentShaderVelocity').textContent, dtVelocity);
        this.positionVariable = this.gpuCompute.addVariable("texturePosition", document.getElementById('fragmentShaderPosition').textContent, dtPosition);
        this.gpuCompute.setVariableDependencies(this.velocityVariable, [this.positionVariable, this.velocityVariable]);
        this.gpuCompute.setVariableDependencies(this.positionVariable, [this.positionVariable, this.velocityVariable]);
        this.positionUniforms = this.positionVariable.material.uniforms;
        this.velocityUniforms = this.velocityVariable.material.uniforms;
        this.positionUniforms.time = {
            value: 0.0
        };
        this.positionUniforms.delta = {
            value: 0.0
        };
        this.velocityUniforms.time = {
            value: 1.0
        };
        this.velocityUniforms.delta = {
            value: 0.0
        };
        this.velocityUniforms.testing = {
            value: 1.0
        };
        this.velocityUniforms.seperationDistance = {
            value: 10.0
        };
        this.velocityUniforms.alignmentDistance = {
            value: 1.0
        };
        this.velocityUniforms.cohesionDistance = {
            value: 20.0
        };
        this.velocityUniforms.freedomFactor = {
            value: 2.0
        };
        this.velocityUniforms.predator = {
            value: new THREE.Vector3()
        };
        this.velocityVariable.material.defines.BOUNDS = BOUNDS.toFixed(2);
        this.velocityVariable.wrapS = THREE.RepeatWrapping;
        this.velocityVariable.wrapT = THREE.RepeatWrapping;
        this.positionVariable.wrapS = THREE.RepeatWrapping;
        this.positionVariable.wrapT = THREE.RepeatWrapping;
        var error = this.gpuCompute.init();
        if (error !== null) {
            console.error(error);
        }
    }

    this.update = () => {
        var now = performance.now();
        var delta = (now - this.last) / 1000;
        if (delta > 1) delta = 1; // safety cap on large deltas
        this.last = now;
        this.positionUniforms.time.value = now;
        this.positionUniforms.delta.value = delta;
        this.velocityUniforms.time.value = now;
        this.velocityUniforms.delta.value = delta;
        this.birdUniforms.time.value = now;
        this.birdUniforms.delta.value = delta;
        // velocityUniforms.predator.value.set(0.5 * mouseX / windowHalfX, -0.5 * mouseY / windowHalfY, 0);
        this.gpuCompute.compute();
        this.birdUniforms.texturePosition.value = this.gpuCompute.getCurrentRenderTarget(this.positionVariable).texture;
        this.birdUniforms.textureVelocity.value = this.gpuCompute.getCurrentRenderTarget(this.velocityVariable).texture;
    }

}