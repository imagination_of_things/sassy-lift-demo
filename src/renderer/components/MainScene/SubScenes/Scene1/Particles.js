import * as THREE from 'three'
import {
    TweenLite
} from 'gsap'


export default class Particles {



    constructor(sceneGroup) {
        this.sceneGroup = sceneGroup;
        this.initParticles();
    }


    initParticles() {
        this.particle = new THREE.Object3D();
        this.hidden = true;
        this.sceneGroup.add(this.particle);
        let geometry = new THREE.TetrahedronGeometry(2, 1);

        let material = new THREE.MeshPhongMaterial({
            color: 0xaaaaaa,
            shading: THREE.FlatShading
        });

        for (var i = 0; i < 150; i++) {
            var mesh = new THREE.Mesh(geometry, material);
            mesh.scale.set(0.2, 0.1, 0.2);
            mesh.position.set(Math.random() - 0.5, Math.random() - 0.5, Math.random() - 0.5).normalize();
            mesh.position.multiplyScalar(20 + (Math.random() * 50));
            mesh.rotation.set(Math.random() * 2, Math.random() * 2, Math.random() * 2);
            this.particle.add(mesh);
        }

        this.particle.position.set(0, 0, 0);
    }

    update(time, positions) {
        if (this.hidden === false) {
            this.particle.rotation.x += positions[0].y * 0.00001;
            this.particle.rotation.y += positions[0].x * 0.00001;
        }
    }

    zoomParticles(value) {
        if (this.particle.position.z < 0) {
            TweenLite.to(this.particle.position, 1, {
                z: 100
            })
        }
    }

    reset() {
        TweenLite.to(this.particle.position, 1, {
            z:100
        })
    }


    show() {
        this.hidden = false;
        //Reset the index
        // TweenLite.to(this.particle.position, 1, {

        //     z: -1000,
        //     onComplete: () => {

        //     }
        // })
        this.particle.position.z = 0;

    }



    hide() {
        // terrain.hide();
        this.hidden = true;
        ////console.log("scene 1 particles hide");
        this.particle.position.z = -10000;

        // TweenLite.to(this.particle.position, 1, {
        //     z: -1000,
        //     onComplete: () => {

        //     }
        // })
    }

    setState(state) {
        if (state === 1) {
            if (this.hidden) {
                this.show();
            }
        } else {
            if (this.hidden === false) {
                this.hide();
            }
        }
    }
}