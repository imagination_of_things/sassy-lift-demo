import * as THREE from 'three'
import Simple1DNoise from '../util/Perlin'
import ModelLoader from '../../SceneManager/ModelLoader'

import Palette from '../Scene2/Colors'

import Particles from './Particles'

import {
    MeshLine,
    MeshLineMaterial
} from 'three.meshline'


const MAX_TREES = 2;
import {
    TweenLite,
    Back,
} from 'gsap'
import OSCManager from '../../OSCManager/OSCManager';
import {
    ipcRenderer
} from 'electron'
import SessionManager from '../../SessionManager/SessionManager';
import AudioManager from '../../AudioManager/AudioManager'


const COLOR = {
    top: '#1538FD',
    bottom: '#00ACC8'
}

export default class SceneOne {


    constructor(scene, camera) {
        this.isRecording = 1;
        this.spinRate = 0.01;

        this.generator = new Simple1DNoise();
        this.opacity = 1.0;
        this.hidden = true;
        this.index = 0;
        this.hasGrown = 0;
        this.handsUp = false;
        this.position = {};
        this.midpoint = {};
        this.resolution = new THREE.Vector2(window.innerWidth, window.innerHeight);
        this.colorCounter = 0;
        this.mixers = [];
        this.mixers2 = [];
        this.mixers3 = [];

        this.debrisMeshes = [];
        this.rootsShown = false;
        this.sessionTime = 0.0;
        // this.mixers3 = [];

        this.startPos1 = THREE.Vector3(-100, 0, -10);
        this.startPos2 = THREE.Vector3(100, 0, -10);

        // This is the overarching file for the scene
        this.sceneGroup = new THREE.Group();
        scene.add(this.sceneGroup);
        this.camera = camera;

        this.debrisGroup1 = new THREE.Group();
        this.debrisGroup2 = new THREE.Group();
        this.debrisGroup3 = new THREE.Group();

        this.leftSoundGroup = new THREE.Group();
        this.rightSoundGroup = new THREE.Group();
        this.recordingGroup = new THREE.Group();
        this.centerSoundGroup = new THREE.Group();

        this.leftSoundGroup.position.set(0, 0, 0);
        this.rightSoundGroup.position.set(0, 0, 0);
        this.recordingGroup.position.set(0, 0, 0);
        this.centerSoundGroup.position.set(0, 0, 0);


        this.debrisConnectionGroup = new THREE.Group();

        // this.sceneGroup.add(this.debrisGroup1);
        // this.sceneGroup.add(this.debrisGroup2);
        // this.sceneGroup.add(this.debrisGroup3);

        // this.sceneGroup.add(this.debrisConnectionGroup);

        this.debrisGroup1.position.set(90, 0, -10);
        this.debrisGroup2.position.set(0, 0, 0);
        this.debrisGroup3.position.set(-90, 0, -10);

        this.debrisConnectionGroup.position.set(0, 0, 0);

        // MESH related
        this.scene1mesh;
        this.clock = new THREE.Clock();
        this.meshes = [];
        this.loaded = false;
        this.wiggleRate = 0.2;
        this.handsupState = false;
        this.treeX = 0;

        this.video = document.getElementById('video');

        this.canvas = document.getElementById("image");
        this.ctx = this.canvas.getContext('2d');
        this.texture = new THREE.VideoTexture(video);
        // this.texture = new THREE.CanvasTexture(this.canvas);

        this.textureLoader = new THREE.TextureLoader();
        // this.spriteGlow = this.textureLoader.load('static/textures/glow.png');
        // this.rootsTexture = this.textureLoader.load('static/textures/wood.jpg');
        // this.bumpTexture = this.textureLoader.load('static/textures/ORM-01.png');
        // this.normalMap = this.textureLoader.load('static/textures/Normals-01.png');



        this.loadAnimationGLTF();
        this.particles = new Particles(this.sceneGroup);
        this.internalIntTime = 0;

        this.handSpheres = [];

        this.makeHandSpheres();

        this.oldDistances = new THREE.Vector2(0, 0);
        this.trees = [];
        this.treeMixers = []

        this.addSceneLights();


    }

    addSceneLights() {
        let light = new THREE.PointLight("#14efff", 1.61);
        light.position.y = 30;
        light.position.x = -150;
        light.position.z = 70;
        this.leftSoundGroup.add(light);

        let light2 = new THREE.SpotLight("#24c43c", 0.76);
        light2.position.y = 30;
        light2.position.x = 150;
        light2.position.z = 70;
        this.leftSoundGroup.add(light2);

        let light3 = new THREE.PointLight("#01ff1c", 0.2);
        light3.position.y = 60;
        light3.position.x = 90;
        light3.position.z = 2;
        this.leftSoundGroup.add(light3);

        let light4 = new THREE.SpotLight("#01ff1c", 0.2);
        light4.position.y = 60;
        light4.position.x = -90;
        light4.position.z = 3;
        this.leftSoundGroup.add(light4);

        // -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

        let light5 = new THREE.PointLight("#1071ff", 1.61);
        light5.position.y = 30;
        light5.position.x = -150;
        light5.position.z = 70;
        this.rightSoundGroup.add(light5);

        let light6 = new THREE.SpotLight("#ff005d", 0.76);
        light6.position.y = 30;
        light6.position.x = 150;
        light6.position.z = 70;
        this.rightSoundGroup.add(light6);

        let light7 = new THREE.PointLight("#1071ff", 0.2);
        light7.position.y = 60;
        light7.position.x = 90;
        light7.position.z = 2;
        this.rightSoundGroup.add(light7);

        let light8 = new THREE.SpotLight("#fff4f9", 0.2);
        light8.position.y = 60;
        light8.position.x = -90;
        light8.position.z = 3;
        this.rightSoundGroup.add(light8);

        // -=-=-=-=-=-=-=-=-=-=-=-==-=-=--=-=-=--==-=-=-=-=-=-=-=--==-=-=-=-=-=-=-=-=-=--==-=-=-=-=-=-=--==-

        let light9 = new THREE.PointLight("#ff041b", 1.2);
        light9.position.y = -12;
        light9.position.x = -160;
        light9.position.z = 70;
        this.recordingGroup.add(light9);

        let light10 = new THREE.SpotLight("#ff041b", 1.2);
        light10.position.y = 12;
        light10.position.x = 160;
        light10.position.z = 70;
        this.recordingGroup.add(light10);

        let light11 = new THREE.PointLight("#ffcaa7", 0.4);
        light11.position.y = 50;
        light11.position.x = 50;
        light11.position.z = 100;
        this.recordingGroup.add(light11);

        let light12 = new THREE.SpotLight("#ffcaa7", 0.4);
        light12.position.y = 50;
        light12.position.x = -50;
        light12.position.z = 100;
        this.recordingGroup.add(light12);


        // -=-=-=-==-=-=-=-=-=-=-=--=-=-==-=-=--=-=-=-==-=-=-=-=--=-==-=-=-=-=-=-----------------------------
        let light13 = new THREE.PointLight("#ee9dff", 0.76);
        light13.position.y = 30;
        light13.position.x = -150;
        light13.position.z = 70;
        this.centerSoundGroup.add(light13);

        let light14 = new THREE.SpotLight("#ebff38", 0.76);
        light14.position.y = 30;
        light14.position.x = 150;
        light14.position.z = 70;
        this.centerSoundGroup.add(light14);

        let light15 = new THREE.PointLight("#ee9dff", 0.2);
        light15.position.y = 60;
        light15.position.x = 90;
        light15.position.z = 2;
        this.centerSoundGroup.add(light15);

        let light16 = new THREE.SpotLight("#ebff38", 0.2);
        light16.position.y = 60;
        light16.position.x = -90;
        light16.position.z = 3;
        this.centerSoundGroup.add(light16);

        this.sceneGroup.add(this.leftSoundGroup);
        this.sceneGroup.add(this.rightSoundGroup);
        this.sceneGroup.add(this.recordingGroup);
        this.sceneGroup.add(this.centerSoundGroup);

    }

    setLightMode1() {

        
        this.leftSoundGroup.visible = false;
        this.rightSoundGroup.visible = true;
        this.recordingGroup.visible = false;
        this.centerSoundGroup.visible = false;

        let container = document.getElementById("container");
        container.style.background = 'linear-gradient(to bottom' + ', ' + '#1c42ff' + ', ' + '#1c42ff' + ')';
        this.debrisGroup1.visible = true;
        this.debrisGroup2.visible = true;
        this.debrisGroup3.visible = true;
        this.particles.show();

    }

    setLightModeCenter() {
        this.leftSoundGroup.visible = true;
        this.rightSoundGroup.visible = false;
        this.recordingGroup.visible = false;
        this.centerSoundGroup.visible = false;

        let container = document.getElementById("container");
        container.style.background = 'linear-gradient(to bottom' + ', ' + '#5bfffe' + ', ' + '#5bfffe' + ')';
        this.debrisGroup1.visible = true;
        this.debrisGroup2.visible = true;
        this.debrisGroup3.visible = true;
        this.particles.show();

    }

    setLightMode2() {
        this.leftSoundGroup.visible = false;
        this.rightSoundGroup.visible = false;
        this.recordingGroup.visible = false;
        this.centerSoundGroup.visible = true;

        let container = document.getElementById("container");
        container.style.background = 'linear-gradient(to bottom' + ', ' + '#ddf754' + ', ' + '#ddf754' + ')';
        this.debrisGroup1.visible = true;
        this.debrisGroup2.visible = true;
        this.debrisGroup3.visible = true;
        this.particles.show();

    }

    setLightModeOff() {
        this.leftSoundGroup.visible = false;
        this.rightSoundGroup.visible = false;
        this.recordingGroup.visible = false;
        this.centerSoundGroup.visible = false;

        // let container = document.getElementById("container");
        // container.style.background = 'linear-gradient(to bottom' + ', ' + '#5bfffe' + ', ' +'#5bfffe' + ')';
        this.debrisGroup1.visible = true;
        this.debrisGroup2.visible = true;
        this.debrisGroup3.visible = true;

    }

    setLightModeRec() {
        this.leftSoundGroup.visible = false;
        this.rightSoundGroup.visible = false;
        this.recordingGroup.visible = true;
        this.debrisGroup1.visible = false;
        this.debrisGroup2.visible = false;
        this.debrisGroup3.visible = false;

        this.spinRate = 0;

        let container = document.getElementById("container");
        container.style.background = 'linear-gradient(to bottom' + ', ' + '#ff1033' + ', ' + '#8b0000' + ')';
    }




    loadAnimationGLTF() {

        let model = ModelLoader.models.find(model => {
            return model.name === 'heart'
        })

        // console.log(model);

        // model.gltf.scene.children[0].material.color
        this.tree = model.gltf;
        // this.sceneGroup.add(this.tree.scene);
        this.tree.scene.position.set(90, 0, 0);
        this.tree.scene.scale.set(7.4, 7.4, 7.4);
        // this.tree.scene.rotation.set(0, 90, 0);



        this.tree1 = this.tree.scene.clone();
        this.tree1.animations = this.tree.animations;
        this.tree1.position.set(-90, 0, 0);

        this.sceneGroup.add(this.tree1);

        this.treeMixer1 = new THREE.AnimationMixer(this.tree1)
        // console.log(this.tree1.animations);
        this.tree1Heartbeat = this.treeMixer1.clipAction(this.tree1.animations[0])
        this.tree1Heartbeat.timeScale = 0.5;
        this.tree1Heartbeat.play();

        this.tree2 = this.tree1.clone();
        this.tree2.animations = this.tree.animations;
        this.tree2.position.set(90, 0, 100);

        this.sceneGroup.add(this.tree2);
        this.treeMixer = new THREE.AnimationMixer(this.tree2)
        this.tree2Heartbeat = this.treeMixer.clipAction(this.tree2.animations[0])
        this.tree2Heartbeat.timeScale = 0.5;

        this.tree2Heartbeat.play();

        this.tree1.visible = false;
        this.tree2.visible = false;
        this.roots = new THREE.Group();

        this.tree1.traverse((node) => {
            if (node.isMesh) {
                node.material.map = this.texture;
                // node.material.color = new THREE.Color("#000000");
                // // node.material.normalMap = this.normalMap;
                node.material.morphTargets = true;
                // node.s
            }
        })


        this.meshes.push(this.tree1);
        this.meshes.push(this.tree2);

        // console.log(this.tree1);



        this.treeMixer.addEventListener('finished', function (e) {
            e.action.stop();
        })
        this.treeMixer1.addEventListener('finished', function (e) {
            e.action.stop();
        })


        // LOAD DEBRIS
        let debris1 = ModelLoader.models.find(model => {
            return model.name === 'debris1'
        })

        let debris2 = ModelLoader.models.find(model => {
            return model.name === 'debris2'
        })

        let debris3 = ModelLoader.models.find(model => {
            return model.name === 'debris3'
        })

        debris1.gltf.scene.scale.set(1, 1, 1);
        debris2.gltf.scene.scale.set(1, 1, 1);
        debris3.gltf.scene.scale.set(1, 1, 1);



        this.reactingAnimations = [
            debris1.gltf.animations[0],
            debris2.gltf.animations[0],
            debris3.gltf.animations[0]

        ]
        this.debris1Positions = [new THREE.Vector3(52-90, 42, 0), new THREE.Vector3(49.50425-90, -19.91726, 0), new THREE.Vector3(94.99844-90, 66.33656, 0), new THREE.Vector3(121.42021-90, -4.65202, 0)]
        this.debris1Scales = [2, 1.4, 1.1, 0.7]
        this.debris2Positions = [new THREE.Vector3(-22.172, -6.883, 0), new THREE.Vector3(-28.051, 31.023, 0), new THREE.Vector3(-10.245, 14.230, 0), new THREE.Vector3(17.021, 52.189, 0)]
        this.debris2Scales = [2,1, 1, 1]

        this.debris3Positions = [new THREE.Vector3(-119.864+90, 65.255, 0), new THREE.Vector3(-69.598+90, 41.639, 0), new THREE.Vector3(-74.362+90, -34.752, 0), new THREE.Vector3(-130.539+90, -3.883, 0)]
        this.debris3Scales = [3,2, 2.5, 1]

        // Make debris 1
        for (let i = 0; i < this.debris1Positions.length; i++) {
            let deb = debris1.gltf.scene.clone();
            deb.position.set(this.debris1Positions[i].x, this.debris1Positions[i].y, this.debris1Positions[i].z );
            deb.children[0].scale.setScalar(this.debris1Scales[i])
            this.debrisGroup1.add(deb);
            // console.log(deb)
            this.mixers.push(new THREE.AnimationMixer(deb))
        }


        // Make debris 2

        for (let i = 0; i < this.debris2Positions.length; i++) {
            let deb = debris2.gltf.scene.children[0].clone();
            deb.position.set(this.debris2Positions[i].x, this.debris2Positions[i].y, this.debris2Positions[i].z );
            deb.scale.setScalar(this.debris2Scales[i])
            this.debrisGroup2.add(deb);
            this.mixers.push(new THREE.AnimationMixer(deb))
        }
        // Make debris 3
        for (let i = 0; i < this.debris3Positions.length; i++) {
            let deb = debris3.gltf.scene.children[0].clone();
            deb.position.set(this.debris3Positions[i].x, this.debris3Positions[i].y, this.debris3Positions[i].z );
            deb.scale.setScalar(this.debris3Scales[i])
            this.debrisGroup3.add(deb);
            this.mixers.push(new THREE.AnimationMixer(deb))
        }










        this.playAnimation(this.reactingAnimations[0], this.reactingAnimations[1], this.reactingAnimations[2]);



    }

    playAnimation(animation, animation2) {
        for (let i = 0; i < this.mixers.length; i++) {
            this.mixers[i].clipAction(animation).play();
        }
    }

    playAnimationIndices(animation, indices) {
        for (let i = indices[0]; i < indices[indices.length - 1]; i++) {
            this.mixers[i].timeScale = 1;
        }
    }

    stopAnimationIndices(animation, indices) {
        for (let i = indices[0]; i < indices[indices.length - 1]; i++) {
            this.mixers[i].timeScale = 0;
        }
    }





    updateTimeScale(animation, animation2,animation3, _timeScale) {
        for (let i = 0; i < this.mixers.length; i++) {
            this.mixers[i].clipAction(animation).timeScale = _timeScale;
        }
        for (let i = 0; i < this.mixers2.length; i++) {
            this.mixers2[i].clipAction(animation2).timeScale = _timeScale;
        }
        for (let i = 0; i < this.mixers2.length; i++) {
            this.mixers2[i].clipAction(animation3).timeScale = _timeScale;
        }
    }


    updateTimeScaleAll(animation, animation2,animation3, timeScales) {
        for (let i = 0; i < this.mixers.length; i++) {
            this.mixers[i].clipAction(animation).timeScale = timeScales[0];
        }
        for (let i = 0; i < this.mixers2.length; i++) {
            this.mixers2[i].clipAction(animation2).timeScale = timeScales[1];
        }
        for (let i = 0; i < this.mixers2.length; i++) {
            this.mixers2[i].clipAction(animation3).timeScale = timeScales[2];
        }
    }

    updateTimeScaleTrees(_timeScale) {
        this.tree1Heartbeat.timeScale = _timeScale;
        this.tree2Heartbeat.timeScale = _timeScale;
    }




    makeRoots(start, end) {
        let diff = end - start;
        let division = 0.1;
        let points = 1980;

        var line = new Float32Array(points);
        for (var j = 0; j < points; j += 3) {
            line[j] = start + division * j;
            line[j + 1] = 0;
            line[j + 2] = 10 * Math.sin(.01 * j);
        }
        this.makeLine(line, 0);

        var line = new Float32Array(points);
        for (var j = 0; j < points; j += 3) {
            line[j] = start + division * j;
            line[j + 1] = 10 * Math.sin(.01 * j);
            line[j + 2] = 0;
        }
        this.makeLine(line, 1);

        var line = new Float32Array(points);
        for (var j = 0; j < points; j += 3) {
            line[j] = start + division * j;
            line[j + 1] = 10 * Math.sin(.01 * j);
            line[j + 2] = 0;
        }
        this.makeLine(line, 2);

        var line = new Float32Array(points);
        for (var j = 0; j < points; j += 3) {
            line[j] = start + division * j;
            line[j + 1] = 10 * Math.sin(.01 * j);
            line[j + 2] = 0;
        }
        this.makeLine(line, 3);
        var line = new Float32Array(points);
        for (var j = 0; j < points; j += 3) {
            line[j] = start + division * j;
            line[j + 1] = 10 * Math.sin(.01 * j);
            line[j + 2] = 0;
        }
        this.makeLine(line, 4);








    }

    makeLine(geo, c) {
        var g = new MeshLine();
        g.setGeometry(geo, function (p) {
            return 0.3;
        });

        var material = new MeshLineMaterial({
            // alphaMap: this.rootsTexture,
            // useAlphaMap: 1,
            color: new THREE.Color(0xffffff),
            opacity: 0.0,
            resolution: this.resolution,
            sizeAttenuation: !false,
            lineWidth: 0.3,
            near: this.camera.near,
            far: this.camera.far,
            transparent: true,
        });
        var mesh = new THREE.Mesh(g.geometry, material);
        this.roots.add(mesh);

    }

    makeHandSpheres() {

        for (let i = 0; i < 2; i++) {
            let geometry = new THREE.TetrahedronGeometry(5, 1);

            var material = new THREE.MeshStandardMaterial({
                color: 0x00ff00,
                opacity: 0.0,
                transparent: true,
            });
            var sphere = new THREE.Mesh(geometry, material);
            sphere.scale.set(0.5, 0.5, 0.5);
            sphere.position.set(0, 0, 0);

            this.sceneGroup.add(sphere);
            this.handSpheres.push(sphere);
        }
    }

    clap() {

    }


    // Animation


    // After 30 seconds reset the mesh.
    resetMeshes() {
        this.index = 0;
        this.wiggleRate = 0.2;
        this.hideRoots();


        this.hasGrown = 0;
        for (let i = 0; i < this.meshes.length; i++) {
            // this.meshes[i].morphTargetInfluences[1] = 0.1;
            // this.meshes[i].morphTargetInfluences[2] = 0.3;
            this.meshes[i].children[0].scale.set(0.4, 0.4, 0.4);
        }

        this.debrisGroup1.scale.set(1, 1, 1);
        this.debrisGroup2.scale.set(1, 1, 1);
        this.particles.reset();
        //console.log("meshes reset");

        // reset roots


    }


    getFaceFromWebcam(position) {
        this.ctx.clearRect(0, 0, canvas.width, canvas.height);

        this.ctx.drawImage(video, position.x / 2 - 100, position.y / 2 - 100, 200, 200, 0, 0, canvas.width, canvas.height);
        let texture = new THREE.CanvasTexture(canvas);
        this.tree1.traverse((node) => {
            if (node.isMesh) {
                node.material.map = this.texture;
                // node.s
            }
        })
        // for (let i = 0; i < meshes.length; i++) {
        //     this.meshes[i].material.map = texture;
        // }
    }



    //Arms up explodes the shape. Once
    explode() {

        this.wiggleRate = 0.01;
        //Set to 1, set to 0 in reset
        this.hasGrown += 1;
        TweenLite.to(this.meshes[0].children[0].scale, 3, {
            ease: Back.easeOut.config(8),
            x: 1.5,
            y: 1.8,

            z: 1.5,

        })
        TweenLite.to(this.meshes[1].children[0].scale, 3, {
            ease: Back.easeOut.config(8),
            x: 1.5,
            y: 1.8,

            z: 1.5,

        })

        TweenLite.to(this.meshes[0].scale, 1, {

            // x: 15,
            // y: 15,
            // z: 15,
            onComplete: () => {
                this.particles.zoomParticles();
                OSCManager.sendMessage('/growth', 1);
                //console.log("You've grown a tree!");

            }
        })


        TweenLite.to(this.meshes[1].scale, 1, {

            // x: 15,
            // y: 15,
            // z: 15,
        })
    






    }




    //Shrink the tree down
    shrink() {

    }

    //Grow the tree back to normal size
    grow() {

    }


    calculateDistanceHandsToHead(positions) {
        let newPosX = THREE.Math.mapLinear(positions[0].x, 1280, 0, -128, 128);
        let newPosY = THREE.Math.mapLinear(positions[0].y, 0, 720, -64, 64);
        let newVect = new THREE.Vector3(newPosX, newPosY, 0);
        let leftWristX = THREE.Math.mapLinear(positions[0].leftWrist.x, 1280, 0, -128, 128)
        let leftWristY = THREE.Math.mapLinear(positions[0].leftWrist.y, 720, 0, -64, 64)
        let rightWristX = THREE.Math.mapLinear(positions[0].rightWrist.x, 1280, 0, -129, 129)
        let rightWristY = THREE.Math.mapLinear(positions[0].rightWrist.y, 720, 0, -64, 64)
        let rightWrist = new THREE.Vector3(rightWristX, rightWristY, 0);
        let leftWrist = new THREE.Vector3(leftWristX, leftWristY, 0);

        this.handSpheres[0].position.lerp(leftWrist, 0.1);
        this.handSpheres[1].position.lerp(rightWrist, 0.1);

        let xDistanceHands = THREE.Math.mapLinear(Math.abs(this.handSpheres[0].position.x - this.handSpheres[1].position.x), 20, 230, 0, 127);
        let yDistanceHands = THREE.Math.mapLinear(Math.abs(this.handSpheres[0].position.y - this.handSpheres[1].position.y), 1, 100, 0, 127)
        xDistanceHands = THREE.Math.clamp(xDistanceHands, 0, 127);
        // yDistanceHands = THREE.Math.clamp(yDistanceHands, 0, 1);


        // let leftHandToHead = THREE.Math.mapLinear(this.handSpheres[0].position.distanceTo(handSpheres[1].position), 0, 100, 0, 1.5);
        // let rightHandToHead = THREE.Math.mapLinear(this.handSpheres[1].position.distanceTo(newVect), 0, 100, 0, 1.5);

        return new THREE.Vector2(xDistanceHands, yDistanceHands)
    }


    //Update loop to perform animations
    update(time, delta, positions) {
        if (this.hidden === false && this.meshes) {

            if (OSCManager.isRecording != this.isRecording) {
                if (OSCManager.isRecording === 0) {
                    this.setLightModeRec();

                } else if (OSCManager.isRecording === 1) {
                    this.setLightMode2();

                }

                this.isRecording = OSCManager.isRecording;
            }

            this.internalIntTime += 1;


            if (positions[0]) {

                // this.roots.forEach(function (l, i) {
                // this.roots.children[0].material.uniforms.visibility.value = true ? (time / 3000) % 1.0 : 1.0;
                // });

                // for(let i=0, l=this.debrisMeshes.length; i<l; i++) {
                //     this.debrisMeshes[i].children[0].position.y += Math.sin(time+(i+1))*0.03;
                // }  



                // if (this.internalIntTime % 50 === 0 || this.internalIntTime === 1) {
                //     this.getFaceFromWebcam(positions[0]);

                // }

                if (this.treeMixer != null) {
                    this.treeMixer.update(delta);
                };


                if (this.treeMixer1 != null) {
                    this.treeMixer1.update(delta);

                };

                for (let i = 0; i < this.mixers.length; i++) {
                    if (this.mixers[i]) {
                        this.mixers[i].update(delta);
                    }
                }

                for (let i = 0; i < this.mixers2.length; i++) {
                    if (this.mixers2[i]) {
                        this.mixers2[i].update(delta);
                    }
                }
           



                this.animateBlobs(positions, this.internalIntTime);

                this.particles.update(time, positions);
                this.isHandsUp(positions);

                this.animateDebris(delta);
                this.animateBlobsWithAudio(this.isRecording);

                if(this.isRecording != 0) {
                    this.checkSector(positions[0]);
                }





                // //Only want this to trigger once -- maybe move this 
                // if (this.handsUp != this.handsupState) {
                //     this.handsupState = this.handsUp;
                //     if (this.handsUp && this.hasGrown === 0) {
                //         this.explode();
                //     } else {}
                // }
            }
        }
    }

    checkSector(position) {

        if (position.x < 500) {
            OSCManager.sendMessage('/bass', 0);
            OSCManager.sendMessage('/drums', 0);
            OSCManager.sendMessage('/keys', 1);
            this.setLightMode1();

            this.playAnimationIndices(this.reactingAnimations[0], [0,1,2,3])
            this.stopAnimationIndices(this.reactingAnimations[1], [4,5,6,7]);

            this.stopAnimationIndices(this.reactingAnimations[2], [8,9,10,11]);



        } else if (position.x >= 500 && position.x < 800) {
            OSCManager.sendMessage('/bass', 1);
            OSCManager.sendMessage('/drums', 1);
            OSCManager.sendMessage('/keys', 1);
            this.setLightModeCenter();
            this.playAnimationIndices(this.reactingAnimations[0], [0,1,2,3, 4, 5, 6, 7, 8, 9, 10, 11])



        } else if (position.x >= 800) {
            OSCManager.sendMessage('/bass', 1);
            OSCManager.sendMessage('/drums', 0);
            OSCManager.sendMessage('/keys', 0);
            this.setLightMode2();
            this.stopAnimationIndices(this.reactingAnimations[0], [0,1,2,3])
            this.stopAnimationIndices(this.reactingAnimations[1], [4,5,6,7]);

            this.playAnimationIndices(this.reactingAnimations[2], [8,9,10,11]);



        }
    }


    animateDebris(delta) {

        if (AudioManager.inited && AudioManager.midMeter) {

            // this.debrisGroup1.rotation.y += AudioManager.getAudioLevel() * 0.1
            // this.debrisGroup2.rotation.y += AudioManager.getAudioLevel() * 0.1
            this.updateTimeScale(this.reactingAnimations[0], this.reactingAnimations[1], this.reactingAnimations[2], AudioManager.getAudioLevel() + 0.3);

        }

        // this.debrisGroup1.rotation.x += OSCManager.eq[0]
        // this.debrisGroup2.rotation.x += OSCManager.eq[0]

    }

    animateBlobsWithAudio(isRecording) {
        // this.meshes[0].children[0].morphTargetInfluences[1] =  AudioManager.getAudioLevel();
        // this.meshes[1].children[0].morphTargetInfluences[1] =  AudioManager.getAudioLevel();
        // this.meshes[0].children[0].morphTargetInfluences[4] =  AudioManager.getAudioLevel();
        // this.meshes[1].children[0].morphTargetInfluences[4] =  AudioManager.getAudioLevel();
        this.updateTimeScaleTrees(AudioManager.getAudioLevel() * 2 + 0.25);

        if (isRecording === 0) {
            if(AudioManager.getAudioLevel() > 0.2) {
                this.tree1.rotation.y += 0.15;
                this.tree2.rotation.y += 0.15;

            } else {
                this.tree1.rotation.y += 0.0;
                this.tree2.rotation.y += 0.0;
            }
 
        } else {
            this.tree1.rotation.y += 0.005;
            this.tree2.rotation.y += 0.005;
     

        }


    }

    // Animate left or right using keys
    animateBlobs(positions, time) {


        let newPosX = THREE.Math.mapLinear(positions[0].x, 1280, 0, -110, -80);
        let newPosY = THREE.Math.mapLinear(positions[0].y, 0, 720, 20, -20);
        let newVect = new THREE.Vector3(newPosX, newPosY, -10);
        let newVect2 = new THREE.Vector3(-newPosX, newPosY, -10);
        let newVect3 = new THREE.Vector3(45+newPosX/2, newPosY, -10);

        // newVect.multiplyScalar(0.1);
        this.meshes[0].position.lerp(newVect, 0.1)
        this.meshes[1].position.lerp(newVect2, 0.1)
        this.debrisGroup1.position.lerp(newVect2, 0.1)
        this.debrisGroup2.position.lerp(newVect3, 0.1)
        this.debrisGroup3.position.lerp(newVect, 0.1)

        // this.roots.position.y = this.meshes[0].position.y;

        //Need to put morphing streching here
        // this.meshes[1].morphTargetInfluences[4] = THREE.Math.mapLinear(distances.right, 0, 100, 0, 1);
        // this.meshes[0].morphTargetInfluences[4] = THREE.Math.mapLinear(distances.right, 0, 100, 0, 1);

        let distances = this.calculateDistanceHandsToHead(positions)
        if (this.internalIntTime % 20 === 0) {
            OSCManager.sendMessage('/filter', Math.floor(distances.x));
        }


        // this.oldDistances.lerp(distances, 0.1);
        // console.log(distances.x);

        // if (distances.x >= 1 && this.rootsShown === false && this.hasGrown === 1) {
        //     setTimeout(() => {
        //         this.showRoots();
        //     }, 2000)
        //     SessionManager.setKeyInteraction(1);
        // } else if (distances.x >= 1.5 && this.hasGrown === 0) {
        //     this.explode();
        //     SessionManager.setKeyInteraction(1);

        // }
        // if (this.hasGrown === 1) {
        //     this.tree1Heartbeat.stop();
        //     this.tree2Heartbeat.stop();
        //     this.meshes[0].children[0].morphTargetInfluences[1] = this.oldDistances.x + this.oldDistances.y;
        //     this.meshes[1].children[0].morphTargetInfluences[1] = this.oldDistances.x + this.oldDistances.y;
        //     for (let i = 0; i < this.roots.children.length; i++) {
        //         if (this.roots.children[i].isMesh) {
        //             if (this.roots.children[i].material) {
        //                 this.roots.children[i].material.uniforms.visibility.value = this.oldDistances.x;
        //                 this.roots.children[i].rotation.x = time * 0.01 + (i * 1.25);

        //                 // this.roots.rotation.x += delta;

        //             }

        //         }
        //     }
        //     // console.log(this.meshes[0].children[0].morphTargetDictionary);
        // } else {
        //     this.tree1Heartbeat.play();
        //     this.tree2Heartbeat.play();
        // }


    }


    showRoots() {
        // this.rootsShown = true;
        // // console.log("roots")

        // for (let i = 0; i < this.roots.children.length; i++) {
        //     if (this.roots.children[i].isMesh) {
        //         // console.log(this.roots.children[i].material)

        //         // this.roots.children[i].material.uniforms.visiblity = 0.0;
        //         TweenLite.to(this.roots.children[i].material, 1.5, {
        //             opacity: 0.4,
        //             onComplete: () => {}
        //         })

        //     }

        // }
    }

    hideRoots() {
        // this.rootsShown = false;
        // for (let i = 0; i < this.roots.children.length; i++) {
        //     if (this.roots.children[i].isMesh) {
        //         this.roots.children[i].material.opacity = 0.0

        //     }
        // }
        // this.roots.children[2].material.opacity = 0.0

        // for (let i = 0; i < this.roots.children.length; i++) {
        //     if (this.roots.children[i].isMesh) {
        //         this.roots.children[i].material.opacity = 0.0

        //     }
        // }
    }

    isHandsUp(positions) {
        if (positions[0]) {
            if (positions[0].leftWrist && positions[0].rightWrist) {
                // 
                this.midpoint = {
                    x: (positions[0].leftWrist.x + positions[0].rightWrist.x) / 2,
                    y: (positions[0].leftWrist.y + positions[0].rightWrist.y) / 2
                }

                // If the midpoint is above > 50 the head
                if (this.midpoint.y < positions[0].y + 50.0) {
                    this.handsUp = true;
                } else {
                    this.handsUp = false;
                }
            }
        }
    }



    show() {
        // scene.fog = new THREE.FogExp2(0xcccccc, 0.002);
        // scene.fog = new THREE.FogExp2(0xcccccc, 0.002);
        // document.getElementById('video_bg1').style.display = "block";
        //  let container = document.getElementById("container");
        // container.style.background = 'linear-gradient(to bottom' + ', ' + '#5BFFFE' + ', ' +'#5BFFFE' + ')';
        this.setLightMode1();



        this.resetMeshes()

        this.camera.position.set(0, 0, 105);

        this.hidden = false;

        //console.log("scene 1 show");
        //Reset the index
        this.index = 0;
        this.camera.lookAt(0, 0, 0);
        // Show the trees
        this.meshes[0].visible = true;
        this.meshes[1].visible = true;
        this.explode();

        this.sceneGroup.position.set(0,0,0);

        // TweenLite.to(this.sceneGroup.position, 0.5, {
        //     x: 0,
        //     y: 0,
        //     z: -10,
        //     onComplete: () => {
        //         this.hidden = false;

        //     }
        // })

        this.particles.show();


        // Show the terrain

    }



    hide() {
        this.hidden = true;
        // document.getElementById('video_bg1').style.display = "none";
        this.setLightModeOff();

        this.particles.hide();
        this.sceneGroup.position.set(0,0,-10000);

        // TweenLite.to(this.sceneGroup.position, 1, {
        //     z: -10000,
        //     onComplete: () => {
        //         this.hidden = true;
        //         //console.log("scene 1 hide");
        //         this.particles.hide();
        //     }
        // })
    }



    setState(state) {

        if (state === 1) {
            this.show();

        } else {
            this.hide();
        }
    }
}