import OSC from 'osc-js'
import SessionManager from '../SessionManager/SessionManager'

class OSCManager {

    constructor(listenerCallback) {
        this.audioStatus = 123;
        this.state = 0;
        this.osc = new OSC();
        this.eq = null;
        this.classification = -1;
        this.osc.open({
            port: 9789
        });

        // Register Listeners for OSC MESSAGES
        this.osc.on('/noiselevel', message => {
            // SessionManager.setAudioLevel(message.args[0]);
        })
        this.osc.on('/silence', message => {})


        this.osc.on('/listening', message => {
            this.audioStatus = message.args[0];
        })

        this.osc.on('/classification', message => {

            this.classification = message.args[0]

            setTimeout(()=>{
                this.classification = 0;
            },500)


        })

        this.osc.on('/eq', message => {
            this.eq = message.args;
        })

        this.osc.on('/recording', message => {
            this.isRecording = message.args[0] ? message.args[0] : 0;
        })
    }



    registerCallback(callback) {
        this.listenerCallback = callback;
    }

    sendState(state) {

        if (state != this.state) {
            let message = new OSC.Message('/state', state);
            console.log("state", state);
            this.osc.send(message);
            this.state = state;
        }

    }

    sendMessage(address, value) {
        let message = new OSC.Message(address, value);
        this.osc.send(message);
    }

    sendPosition(position) {

        let message = new OSC.Message('/pose1/x', Math.round(position.x));
        let message2 = new OSC.Message('/pose1/y', Math.round(position.y));

        this.osc.send(message);
        this.osc.send(message2);

    }


}

export default new OSCManager();