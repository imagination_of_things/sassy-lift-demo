"use strict"


//The SceneManager is responsible for handling the Three.js side of the app,
//which is completely hidden from the main. It knows nothing about the DOM.
import * as THREE from 'three'

//Scene subjects --- Each of the Three scenes for each state.
import Scene1 from '../SubScenes/Scene1/Scene1'
import Scene2 from '../SubScenes/Scene2/Scene2'
import Scene3 from '../SubScenes/Scene3/Scene3'

import SessionManager from '../SessionManager/SessionManager'

// // Abstracting away the loading of models.
// import ModelLoader from './ModelLoader'
import {
    BloomEffect,
    EffectComposer,
    EffectPass,
    BlurPass,
    RenderPass,
    BlendFunction,
    NoiseEffect
} from "postprocessing";


// Tweenlite for nice tweening of variables
import {
    TweenLite
} from 'gsap'

// Clap Detector
import Recording from '../ClapDetector/ClapDetection'

import {
    Clock
} from 'three'
import {
    MAX_UNSIGNED_VALUE
} from 'long';
import OSCManager from '../OSCManager/OSCManager';

const yellow = {
    top: '#007D56',
    bottom: '#00ACC8'
}

// -----------------------------------------------------------------------------
export default class SceneManager {

    constructor(canvas) {
        // This holds the state number - 0-3
        this.state = -1;
        this.hidden = true;
        this.scenes = [];
        this.gameState = -1;
        this.classification = -1;

        this.clock = new THREE.Clock();

        // SETUP VARIABLES for canvas
        let container = document.getElementById('container');
        canvas.width = container.clientWidth;
        canvas.height = container.clientHeight;
        this.width = canvas.width;
        this.height = canvas.height;
        this.size = {
            width: this.width,
            height: this.height
        }


        //Event variables from Posenet
        this.handsUp = false;
        this.positions = [];

        // Animation variables
        this.time = 0;
        this.oldTime = 0;

        //Setup Scene, Lights, Camera and Renderer
        this.scene = new THREE.Scene();
        this.lights = this.buildLights(this.scene);
        this.camera = this.buildCamera(this.width, this.height);
        this.renderer = this.buildRender(this.width, this.height);

        this.initScenes();

        container.style.background = 'linear-gradient(to bottom' + ', ' + yellow.top + ', ' + yellow.bottom + ')';



    }





    // Load the scenes 1-3, self contained
    initScenes() {

        this.scene1 = new Scene1(this.scene, this.camera);
        this.scenes.push(this.scene1);

        this.scene2 = new Scene2(this.scene);
        this.scenes.push(this.scene2);

        this.scene3 = new Scene3(this.renderer, this.scene, this.size);
        this.scenes.push(this.scene3);
        this.setupClapDetection();
    }

    // Setup function for clap detection
    setupClapDetection() {
        var lastClap = (new Date()).getTime();

        function detectClap(data) {
            var t = (new Date()).getTime();
            if (t - lastClap < 200) return false; // TWEAK HERE
            var zeroCrossings = 0,
                highAmp = 0;
            for (var i = 1; i < data.length; i++) {
                if (Math.abs(data[i]) > 0.5) highAmp++; // TWEAK HERE
                if (data[i] > 0 && data[i - 1] < 0 || data[i] < 0 && data[i - 1] > 0) zeroCrossings++;
            }
            if (highAmp > 60 && zeroCrossings > 100) { // TWEAK HERE
                ////console.log(highAmp+' / '+zeroCrossings);
                lastClap = t;
                return true;
            }
            return false;
        }


        var rec = new Recording((data) => {

            // Clap detected here
            if (detectClap(data)) {
  
                if (this.state === 1 || this.state === 2) {
                    this.scene2.clap();
                }

            }
        });
    }

    clap() {
        this.scene2.clap();

    }

    grenade() {
        if(this.state === 3) {
            this.scene2.multiCreatures.demoExplode();

        }
    }


    // BUILD LIGHTS
    buildLights(scene) {
        var light = new THREE.SpotLight("#aaaaaa", 1.0);
        light.position.y = 1000;
        light.position.x = 1000;
        light.position.z = 100;
        scene.add(light);

        var light2 = new THREE.SpotLight("#aaaaaa", 1.0);
        light2.position.y = -1000;
        light2.position.x = -1000;
        light2.position.z = 100;
        scene.add(light2);

        var hemi = new THREE.HemisphereLight("#ffffff", 1000);
        hemi.position.set(100, 1000, 100);
        // scene.add(hemi);

        return [light, light2];
    }

    // BUILD CAMERA

    buildCamera(width, height) {
        var aspectRatio = width / height;
        var fieldOfView = 75;
        var nearPlane = 1;
        var farPlane = 1200;
        var camera = new THREE.PerspectiveCamera(fieldOfView, aspectRatio, nearPlane, farPlane);

        // // var camera = new THREE.OrthographicCamera( width / - 2, width / 2, height / 2, height / - 2, 1, 1000 );
        // var camera = new THREE.OrthographicCamera( width * aspectRatio / - 2, 0.5 * height * aspectRatio / 2, width / 2, height / - 2, 1, 1200 );
        // // cameraOrthoHelper = new THREE.CameraHelper( cameraOrtho );
        // // scene.add( cameraOrthoHelper );

        // camera.position.set(width, height, 0);
        camera.lookAt(new THREE.Vector3(0, 0, 0));
        return camera;
    }

    //BUILD RENDER
    buildRender(width, height) {
        var renderer = new THREE.WebGLRenderer({
            canvas: canvas,
            antialias: true,
            alpha: true
        });
        var DPR = (window.devicePixelRatio) ? window.devicePixelRatio : 1;
        renderer.setPixelRatio(DPR);
        renderer.setSize(width, height);
        renderer.setClearColor(0x000000, 0.0);

        renderer.gammaInput = true;
        renderer.gammaOutput = true;

        return renderer;
    }


    // THIS IS CALLED FROM main.js
    setActivity(value) {
        this.activityLevel = value;
        if (this.activityLevel <= 0.25) {
            this.isActivity = true;
        } else {
            this.isActivity = true;
        }
    }


    // ?
    updateCameraAndLights(value) {
        camera.lookAt(value, 0, 0);
        camera.position.x = value;
        lights[0].position.x = value;
        lights[1].position.x = value;
    }

    // UPDATE LOOP - called from render in main.js
    update(delta) {
        if (this.time < MAX_UNSIGNED_VALUE) {
            this.time += delta;
        } else {
            this.time = 0;
            this.time += delta;
        }

        // ////console.log("delta", delta*10);
        this.scene2.update(this.time, delta, this.positions);

        if(Math.abs(this.time-this.oldTime)> 0.5) {
            this.oldTime = this.time;
            if (OSCManager.classification === 1) {
                this.scene2.clap();
    
            } else if (OSCManager.classification === 2) {
                this.scene2.cough();
    
            }
    
        }


        // switch (this.state) {
        //     case 0:

        //         break;

        //     case 1:
        //         this.scene1.update(this.time, delta, this.positions);

        //         break;
        //     case 2:

        //         break;
        //     case 3:
        //         this.scene3.update(this.time, this.camera, delta, this.positions);

        //         break;

        //     default:
        //         break;
        // }
        this.renderer.render(this.scene, this.camera);


    };

    hide() {

    }


    show() {


    }

    onWindowResize() {
        var canvas = document.getElementById("canvas");
        var width = document.body.clientWidth;
        var height = document.body.clientHeight;
        canvas.width = width;
        canvas.height = height;

        camera = buildCamera(width, height);

        renderer.setSize(width, height);
    }



    setState(state) {
        if (state != this.state) {
            console.log("state scene manager", state);
            this.state = state;




            this.scene1.setState(0., this.camera);
            this.scene2.setState(this.state, this.camera);
            this.scene3.setState(0, this.camera);




            this.lights[0].intensity = 1.0;
            this.lights[1].intensity = 1.0;

        }
    }

    setGameState(state) {
        this.scene2.setGameState(state);
    }

    setPositions(posePos) {
        for (let i = 0; i < posePos.length; i++) {
            if (posePos[i]) {
                if (isNaN(posePos[i].x) || isNaN(posePos[i].y)) {

                } else {
                    this.positions[i] = posePos[i];

                }
            }
        }
    }
}