import * as THREE from 'three'
import GLTFLoader from 'three-gltf-loader';

class ModelLoader {

    static instance;

    //Files to load

    constructor() {
        this.files = [
            'static/models/gltf/Heart/Heart.gltf',
            'static/models/gltf/Laser/Mode-02.gltf',
            'static/models/gltf/Banana-Shrink/Fruits-06.gltf',
            'static/models/gltf/Mode-03-Treble/Mode-03-Treble.gltf',
            'static/models/gltf/Mode-03-Mid/Mode-03-Mid.gltf',
            'static/models/gltf/Mode-03-Base/Mode-03-Base.gltf',
            'static/models/gltf/New-Debris-01/Debris01.gltf',
            'static/models/gltf/New-Debris-02/Debris02.gltf',
            'static/models/gltf/Debris-03/Debris03.gltf',
            'static/models/gltf/Pepper/pepper.gltf',
            'static/models/gltf/Avocado/Fruits-05.gltf',
            'static/models/gltf/Pineapple/Pineapple-03.gltf',

        ];

        this.modelNames = [
            'heart',
            'laser',
            'banana', 
            'treble',
            'mid',
            'bass',
            'debris1',
            'debris2',
            'debris3',
            'pepper',
            'avocado',
            'pineapple'

        ]
        this.index = 0;
        this.models = [];
        this.loader = new GLTFLoader();
        this.instance = this;
    }


    // Load all models, resolve when finished.



    //Recursive function 
    loadFiles() {
        let promise = new Promise((resolve, reject) => {
            let counter = 0;
            for (let i = 0; i < this.files.length; i++) {
                
                // Load a glTF resource
                this.loader.load(
                    // resource URL
                    this.files[i],
                    // called when the resource is loaded
                    (gltf) => {

                        let model = {};
                        model.name = this.modelNames[i];
                        model.gltf = gltf;
                        this.models.push(model);
                        //////console.log(model);
                        counter+=1;
                        // We've reached the final file.
                        if (counter === this.files.length) {
                            resolve();
                        }



                    },
                    // called while loading is progressing
                    function (xhr) {

                        //////console.log((xhr.loaded / xhr.total * 100) + '% loaded');

                    },
                    // called when loading has errors
                    function (error) {

                        //console.log('An error happened');
                        //console.log(error);
                        reject();

                    }
                );
            }
        })
        return promise;
    }




}

export default new ModelLoader();