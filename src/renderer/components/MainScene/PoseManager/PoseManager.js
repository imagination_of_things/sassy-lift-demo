"use strict"

import ml5 from 'ml5'

import arrayAverage from '../libs/ArrayAverage.js'
import arrayAverageInt from '../libs/ArrayAverage.js'
import getBoundingBox from './BoundingBox'
import * as THREE from 'three';

const BUFFER_SIZE = 32;
const CENTROID_BUFFER_SIZE = 8;

const LT_BUFFER_SIZE = 64;

const MAX_MESHES = 5;

import SessionManager from '../SessionManager/SessionManager'

import oscManager from '../OSCManager/OSCManager'
import {
    runInThisContext
} from 'vm';

export default class PoseManager {

    constructor(activityCallback, onNewPosition) {

        //To store poses data
        this.poses = [];

        // Buffers to average out values;
        this.activityBuffer = [];
        this.activityBufferLT = [];
        this.areaBuffers = [];
        this.noseBuffers = [];
        this.leftHandBuffers = [];
        this.rightHandBuffers = [];
        this.centroidBuffers = [];
        this.accelerationBuffers = [];
        this.positions = [];
        this.prevCentroids = [];

        this.motionCounter = 0;
        this.skeletonCounter = 0;

        this.activityCallback = activityCallback;
        this.onNewPosition = onNewPosition;
        // The array of centroid
        this.centroids = [];
        this.prevCentroid = {
            x: 0,
            y: 0
        };
        this.motionSpeed = 0;

        // For value mapping purposes;
        let container = document.getElementById('container');
        this.width = container.clientWidth;
        this.height = container.clientHeight;

        // If there are poses
        this.isPoses = false;

        //To setup hardware - usin getVideo, getVideoOld --> for laptop webcam.
        this.audio;
        this.video = this.getVideoOld();


        // For Motion Speed

        //use date to prevent multiple shakes firing
        this.lastTime = new Date();
        this.options = {
            threshold: 40,
            timeout: 1500
        }
        //create custom event
        if (typeof document.CustomEvent === 'function') {
            this.event = new document.CustomEvent('shake', {
                bubbles: true,
                cancelable: true
            });
        } else if (typeof document.createEvent === 'function') {
            this.event = document.createEvent('Event');
            this.event.initEvent('shake', true, true);
        } else {
            return false;
        }
    }


    motionReset() {
        this.lastTime = new Date();
        this.lastX = null;
        this.lastY = null;
        this.lastZ = null;
    }

    getPosesFromOtherWindow(results) {
        this.poses = results;
        // Handle poses function seperates out the keypoints we need.
        this.handlePoses(results, this.activityCallback, this.onNewPosition);

        if (this.motionCounter > 5 && this.motionSpeed > 0) {

            SessionManager.setMotionSpeed(this.motionSpeed);
            this.motionCounter = 0;
        } else {
            this.motionCounter += 1;
        }
    }


    // initPosetNetOld() {

    //     // Setup the promise to get Posenet
    //     let promise1 = new Promise((resolve, reject) => {

    //         // Get video
    //         this.video = this.getVideoOld();



    //         //Start posenet on our video stream
    //         this.poseNet = ml5.poseNet(this.video, 'multiple', () => {
    //             ////console.log("posenet LOADED")
    //             // Here we resolve the promise
    //             resolve();
    //         });

    //         // Setup the callback for new results
    //         this.poseNet.on('pose', (results) => {
    //             this.poses = results;
    //             // Handle poses function seperates out the keypoints we need.
    //             this.handlePoses(results, activityCallback, onNewPosition);

    //             if (this.motionCounter > 5 && this.motionSpeed > 0) {

    //                 SessionManager.setMotionSpeed(this.motionSpeed);
    //                 this.motionCounter = 0;
    //             } else {
    //                 this.motionCounter += 1;
    //             }
    //         });
    //     });

    //     return promise1;

    // }




    // If new camera plugged in ------ Need a permananet solution for changing device id
    getVideo() {

        var video = document.getElementById('video');
        var audio = {};
        var cam = {};

        navigator.mediaDevices.enumerateDevices()
            .then(gotDevices).then(getStream).catch(handleError);

        function gotDevices(deviceInfos) {
            for (let i = 0; i !== deviceInfos.length; ++i) {
                const deviceInfo = deviceInfos[i];
                if (deviceInfo.kind === 'audioinput') {
                    // ////console.log(deviceInfo);

                    if (deviceInfo.label.indexOf("BRIO 4K") >= 0) {
                        ////console.log(deviceInfo);

                        // audio.value = deviceInfo.deviceId;
                    }
                    // audio.value = 'b747d0669cb7ae9460401ac40d1a50f07205bf12007e5329df63c22838eac41b';

                } else if (deviceInfo.kind === 'videoinput') {

                    if (deviceInfo.label.indexOf("BRIO 4K") >= 0) {
                        ////console.log(deviceInfo);

                        cam.value = deviceInfo.deviceId;
                    }

                    // cam.value = 'f0f6174af2879d2293ddd14d14520a9a1f19931d9df18b69b0b1b38a35faf6e7';
                } else {
                    // ////console.log('Found another kind of device: ', deviceInfo);
                }
            }
        }

        function getStream() {
            if (window.stream) {
                window.stream.getTracks().forEach(function (track) {
                    track.stop();
                });
            }

            const constraints = {
                // audio: {
                //     deviceId: {
                //         exact: audio.value
                //     }
                // },
                video: {
                    deviceId: {
                        exact: cam.value
                    }
                }
            };

            navigator.mediaDevices.getUserMedia(constraints).
            then(gotStream).catch(handleError);
        }

        function gotStream(stream) {
            window.stream = stream; // make stream available to console
            video.srcObject = stream;
            video.play();
        }

        function handleError(error) {
            console.error('Error: ', error);
        }


        return video;


    }


    // For just laptop webcam.
    getVideoOld() {
        // // Create a webcam capture
        var video = document.getElementById('video');

        if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
            //console.log(navigator.mediaDevices)
            navigator.mediaDevices.getUserMedia({
                video: true
            }).then(function (stream) {
                video.src = window.URL.createObjectURL(stream);
                // video.src = 'static/twopeople.mp4';
                // video.loop = true;
                video.play();
            });
        }
        return video;
    }

    //Calculate activity of scene to set state.
    calculateActivity(scoredPoses, activityCallback) {

        if (this.activityBuffer.length < BUFFER_SIZE) {
            this.activityBuffer.push(scoredPoses);
        } else {
            this.activityBuffer.shift();
            this.activityBuffer.push(scoredPoses);
        }

        if (this.activityBufferLT.length < LT_BUFFER_SIZE) {
            this.activityBufferLT.push(scoredPoses);
        } else {
            this.activityBufferLT.shift();
            this.activityBufferLT.push(scoredPoses);
        }

        if (this.activityBuffer != null && this.activityBuffer.length === BUFFER_SIZE) {
            // Return the value to main.js to set state.
            activityCallback(this.arrayAverageInt(this.activityBuffer), this.arrayAverageInt(this.activityBufferLT));
        }
    };


    handlePoses(results, activityCallback, positionsCallback) {
        let scoredPoses = 0;
        let motionSpeed = 0;

        // if (this.skeletonCounter > 250) {

        //     SessionManager.uploadSampleData(results)
        //     // //console.log(results);
        //     this.skeletonCounter = 0;

        // } else {
        //     this.skeletonCounter += 1;
        // }


        for (let i = 0, l = results.length; i < l; i++) {
            if (results[i]) {



                // If the pose has a sufficient score, add to the scoredPoses
                if (results[i].pose.score > 0.25) {
                    scoredPoses += 1;
                    //Calculate centroid: five points nose ears and eyes;
                    let centroid = this.calculateCentroid(results[i].pose);
                    if (!this.centroidBuffers[i]) {
                        this.centroidBuffers.push([])

                    } else {
                        if (this.centroidBuffers[i].length < CENTROID_BUFFER_SIZE) {
                            this.centroidBuffers[i].push(centroid);

                        } else {
                            this.centroidBuffers[i].shift();
                            this.centroidBuffers[i].push(centroid);

                        }

                    }


                    this.calculateShake(this.centroidBuffers[i])



                    // From the centroid we need to listen for shake value;
                    if (i === 0 && this.skeletonCounter % 20 === 0) {

                        let position = {
                            x: THREE.Math.mapLinear(centroid.x, 0, 1280, 1, 127),
                            y: THREE.Math.mapLinear(centroid.y, 720, 0, 1, 127)
                        }
                        oscManager.sendPosition(position);
                    }



                    this.positions[i] = {
                        user: i,
                        x: centroid.x,
                        y: centroid.y,
                        // velocity : v,
                        leftWrist: {
                            x: results[i].pose.keypoints[9].position.x,
                            y: results[i].pose.keypoints[9].position.y,
                        },
                        rightWrist: {
                            x: results[i].pose.keypoints[10].position.x,
                            y: results[i].pose.keypoints[10].position.y,
                        },
                    }

                    if (!this.leftHandBuffers[i]) {
                        this.leftHandBuffers.push([])

                    } else {
                        if (this.leftHandBuffers[i].length < CENTROID_BUFFER_SIZE) {
                            this.leftHandBuffers[i].push(this.positions[i].leftWrist);

                        } else {
                            this.leftHandBuffers[i].shift();
                            this.leftHandBuffers[i].push(this.positions[i].leftWrist);

                        }

                    }
                    if (!this.rightHandBuffers[i]) {
                        this.rightHandBuffers.push([])

                    } else {
                        if (this.rightHandBuffers[i].length < CENTROID_BUFFER_SIZE) {
                            this.rightHandBuffers[i].push(this.positions[i].rightWrist);

                        } else {
                            this.rightHandBuffers[i].shift();
                            this.rightHandBuffers[i].push(this.positions[i].rightWrist);

                        }

                    }
                    this.calculateShake(this.leftHandBuffers[i])

                    this.calculateShake(this.rightHandBuffers[i])


                }
            }

            // Averaging over all poses
        }

        // Add activity to buffer - when buffer full - send average to main.js
        this.calculateActivity(scoredPoses, activityCallback);
        this.motionSpeed = motionSpeed / results.length;
        positionsCallback(this.positions)
    }

    calculateMotionSpeed(prevCentroid, centroid, index) {



        let dX = Math.abs(prevCentroid.x - centroid.x);
        let dY = Math.abs(prevCentroid.y - centroid.y);
        prevCentroid = centroid;

        //If one of the skeletons has shaken 
        if (((dX > this.options.threshold) || (dY > this.options.threshold))) {

            //calculate time in milliseconds since last shake registered
            let currentTime = new Date();
            let timeDifference = currentTime.getTime() - this.lastTime.getTime();

            if (timeDifference > this.options.timeout) {
                if (centroid.x > 50 && centroid.x < 1200) {
                    window.dispatchEvent(this.event);
                    this.lastTime = new Date();
                }

            }
        }


        return ((dX * dX) + (dY * dY))
    }

    calculateShake(buffer) {
        var signsDiffer = function (a, b) {
            return (a >>> 63 !== b >>> 63);
        };
        // Idea is to check if velocity has changed direction three times

        // [ 0 1 2 3 4 5 6 7 8]
        let deltaDirection = {
            x: 0,
            y: 0,
        };

        let velocityBuffer = []


        for (let i = buffer.length - 1; i >= 2; i--) {

            // First step
            let vx = buffer[i].x - buffer[i - 1].x
            let vy = buffer[i].y - buffer[i - 1].y

            let vx2 = buffer[i - 1].x - buffer[i - 2].x
            let vy2 = buffer[i - 1].y - buffer[i - 2].y


            if (isNaN(vx) || isNaN(vy) || isNaN(vx2) || isNaN(vy2)) {

            } else {

                // check for sign change

                if (Math.abs(vx - vx2) > this.options.threshold && signsDiffer(vx, vx2)) {
                    deltaDirection.x += 1
                }

                if (Math.abs(vy - vy2) > this.options.threshold && signsDiffer(vy, vy2)) {
                    deltaDirection.y += 1
                }
            }

        }

        //console.log(deltaDirection)

        if (deltaDirection.x >=4 || deltaDirection.y >= 8) {
            let currentTime = new Date();
            let timeDifference = currentTime.getTime() - this.lastTime.getTime();

            if (timeDifference > this.options.timeout) {
                window.dispatchEvent(this.event);
                this.lastTime = new Date();


            }
        }


        // for(let j=)



    }

    // Five points
    calculateCentroid(pose) {
        let x = 0.0;
        let y = 0.0;

        let centroid = {
            x: 0,
            y: 0
        }
        for (let i = 0; i < 5; i++) {
            x += pose.keypoints[i].position.x;
            y += pose.keypoints[i].position.y;
        }
        centroid.x = x * 0.2;
        centroid.y = y * 0.2;
        return centroid;

    }



    //Help to add keypoint to its buffer array
    // Note that each bufferArray is an array of buffers
    addToBufferArray(bufferArray, score, part, i) {
        if (bufferArray[i]) {
            if (score > 0.3) {
                if (bufferArray[i].length < BUFFER_SIZE) {
                    bufferArray[i].push(part);
                } else {
                    bufferArray[i].shift();
                    bufferArray[i].push(part);
                }
            }
        } else {
            bufferArray[i] = []
        }
    }

    averageAreas() {

        if (this.areaBuffers) {
            for (let i = 0; i < this.areaBuffers.length; i++) {
                let a = this.arrayAverageInt(this.areaBuffers[i])

            }

        }
    }



    //Helper for ints
    arrayAverageInt(array) {
        let xsum = 0;
        for (var i = 0; i < array.length; i++) {
            xsum += array[i]
        }

        if (isNaN(xsum / array.length)) {
            return 0;
        } else {
            return xsum / array.length

        }
    }



    calculateVelocity(array) {
        let vxArray = [];
        let vyArray = [];

        for (var i = 1; i < array.length; i++) {

            let vx = array[i].position.x - array[i - 1].position.x
            let vy = array[i].position.y - array[i - 1].position.y


            if (isNaN(vx) || isNaN(vy)) {

            } else {
                vxArray.push(vx);
                vyArray.push(vy);
            }
        }

        let averageVelocity = {
            vx: this.arrayAverageInt(vxArray),
            vy: this.arrayAverageInt(vyArray)
        }


        return averageVelocity;


    }
}