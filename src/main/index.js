import {
  app,
  BrowserWindow,
  ipcMain,
  globalShortcut
} from 'electron'
import Server from './OSCServer/Server'
import axios from 'axios'
const URL = 'https://sassy-lift.herokuapp.com/api/'
const config = {
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
  }
}

import fs from 'fs'
const storeData = (data, path) => {
  try {
    fs.writeFileSync(path, JSON.stringify(data))
  } catch (err) {
    console.error(err)
  }
}



let loadTime = Date.now();
//Four hours
const refreshTime = 14400000;
// const refreshTime = 60000;

const server = new Server();

// RELOAD

if (process.env.NODE_ENV !== 'development') {
  global.__static = require('path').join(__dirname, '/static').replace(/\\/g, '\\\\')
}

let mainWindow;
let win;

const winURL = process.env.NODE_ENV === 'development' ?
  `http://localhost:9080` :
  `file://${__dirname}/index.html`

function createWindow() {
  /**
   * Initial window options
   */
  mainWindow = new BrowserWindow({
    height: 563,
    useContentSize: true,
    width: 1000,
    alwaysOnTop: true
  })

  server.start();
  server.startHttp();
  server.hello();

  mainWindow.loadURL(winURL)
  mainWindow.maximize();
  mainWindow.setFullScreen(true);

  mainWindow.on('closed', () => {
    mainWindow = null
    server.stop();
  })



  mainWindow.webContents.on("devtools-opened", () => {
    mainWindow.webContents.closeDevTools();
  });
}

function createBackgroundProcess() {
  // const invisPath = 'file://' + path.join(__dirname, 'static/inv.html');
  const invisPath = process.env.NODE_ENV === 'development' ?
    `http://localhost:9080/static/inv.html` :
    `file://${__dirname}/static/inv.html`



  win = new BrowserWindow({
    width: 400,
    height: 400,
    show: false
  });

  win.loadURL(invisPath);

  win.webContents.on('did-finish-load', function () {
    //win.show();
  });

  ipcMain.on('dom-is-ready', function (event) {
    const input = 100;

  });


  // We get the poses on main here.
  ipcMain.on('raw-poses', function (event, input) {
    // const message = `The  of ${input} is ${output}`
    // console.log(input);
    mainWindow.webContents.send('posenet-data', input);


    // If been operating 4 hrs refresh
    if(Date.now() - loadTime >= refreshTime) {
      // console.log("time elapsed:", Date.now() - loadTime);
      mainWindow.reload();
      win.reload();
      loadTime = Date.now();
    }
  });

  ipcMain.on('posenet-loaded', function (event, input) {
    // const message = `The  of ${input} is ${output}`
    // ipcMain.
    mainWindow.webContents.send('threejs-posenet-loaded', true);
    console.log("loaded");
  });


  ipcMain.on('camera-data', function (event, data) {
    // const message = `The  of ${input} is ${output}`
    // ipcMain.
    mainWindow.webContents.send('camera-data-input', data);
  });

}

// MAYBE THIS SETS THE RAM BIGGER


app.on('ready', () => {
  createWindow()
  createBackgroundProcess();
  globalShortcut.register('f5', function () {
    console.log('f5 is pressed')
    mainWindow.reload()
    win.reload();
  })


  globalShortcut.register('CommandOrControl+R', function () {
    console.log('CommandOrControl+R is pressed')
    mainWindow.reload()
    win.reload();
  })
})

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow()
  }
})

ipcMain.on('refresh-app', (event, arg) => {
  win.reload();
  mainWindow.reload();
})


ipcMain.on('session-data', (event, arg) => {

  // Save file locally
  // storeData(arg, 'static/logs/logsession.json');

  // console.log(arg);

  // axios.post(URL + 'sessions/', {
  //     "session": arg
  //   }, config).then(function (response) {
  //     // console.log(response.data);
  //   })
  //   .catch(function (error) {
  //     console.log("SessionDATA", error);
  //   });
  // event.sender.send('session-reply', 'received session')
  // console.log(arg) // prints "ping"

})


// Callback for sample data;
ipcMain.on('sample-data', (event, arg) => {

  // if (arg.skeletons) {
  //   if (arg.skeletons.length === 0) {
  //     return;
  //   }
  // }

  // let sample = {
  //   "sample": arg
  // }
  // // storeData(sample, 'static/logs/logs.json');


  // axios.post(URL + 'samples/', {
  //     "sample": arg
  //   }, config).then(function (response) {
  //     // console.log(response.data);
  //     event.sender.send('sample-reply', 'received sample')
  //   })
  //   .catch(function (error) {
  //     console.log("sample data", error);
  //   });
})